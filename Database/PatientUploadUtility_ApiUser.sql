  
  
 -- Add API Securty access for new application 
DECLARE @caregiverId AS INT
DECLARE @roleId AS INT
DECLARE @tokenSiteId AS INT
DECLARE @AdmiUserId AS INT
DECLARE @oauthAppId AS INT
DECLARE @ApplicationKey as varchar(50) 
DECLARE @ApplicationSecret as varchar(50)
DECLARE @SessionToken as varchar(50)
DECLARE @APIUserName as varchar(50)    

SET @APIUserName = 'SystemUpload';
SET @ApplicationKey = '6B74259B-0F36-4222-A92B-0F41D6972CC0';
SET @ApplicationSecret = 'E7E63CED-FEAA-464B-9530-1704B530BE0E';
SET @SessionToken = 'E26038FC-F885-41CD-92BC-BF7C49C6F0B9'; 	-- site 1
--SET @SessionToken = 'E26038FC-F885-41CD-92BC-BF7C49C6F0B2'; 	-- site 2
--SET @SessionToken = 'E26038FC-F885-41CD-92BC-BF7C49C6F0B3'; 	-- site 3
SET @tokenSiteId = '1'; -- change this to 2 if using site 2 and 3 for site3 and use the token ending with B2/B3 respectively
        
SELECT @AdmiUserId = Id FROM [TblCaregiver] WHERE [User_Name] = 'VivifySystemUser'
        
SELECT @caregiverId = Id FROM [TblCaregiver] Where [User_Name] = @APIUserName;
SELECT @roleId = SecurityRoleId FROM [SecurityRole] Where [Name] = @APIUserName;
        
IF ( @roleId IS NULL )
BEGIN
    PRINT 'Create new role'
    INSERT  [SecurityRole] ( [Name], [IsDeleted], [IsApiOnlyRole], [SortOrder], [Description], [SecurityActivityId])
    VALUES (@APIUserName, 0, 1, 11, 'API access for ' + @APIUserName, 1038)  
    SET @roleId = SCOPE_IDENTITY()
END
          
IF ( @caregiverId IS NULL )
BEGIN
    PRINT 'create new caregiver'
    INSERT [TblCaregiver] ([First_Name],     [Last_Name], [IsSystemUser],     [User_Name],     [Password], [IsDeleted],  [CreatedBy], [FailedLoginAttempts],  [PasswordLastResetUTC], [CreatedDtUTC],  [TimeZoneId], [IsPasswordInNewAlgo], [ReceiveEmail], [AuthorizationModeId])
    VALUES (N'mpi', @APIUserName,              1, @APIUserName,    N'Disabled',           0,  @AdmiUserId,                     0,            GETUTCDATE(),   GETUTCDATE(),             5,                    1, 0, 1)
    SET @caregiverId = SCOPE_IDENTITY()
END


INSERT INTO CaregiverSite_Rel (CaregiverId, SiteId, CreatedDateTime_UTC, CreatedBy_Id) 
	SELECT @caregiverId, SiteId, GETUTCDATE(), @AdmiUserId
	FROM dbo.Site
	WHERE SiteId NOT IN (SELECT CaregiverSite_Rel.SiteId 
									   FROM CaregiverSite_Rel 
									   WHERE CaregiverSite_Rel.CaregiverId = @caregiverId) 


INSERT INTO [TblCaregiverHospitalREL] ([CaregiverId], [HospitalId], [CreatedBy], [CreatedDtUTC], [IsDeleted] )
	SELECT @caregiverId, TblHospitalMaster.Id, @AdmiUserId, GETUTCDATE(), 0 from dbo.TblHospitalMaster
	WHERE TblHospitalMaster.Id NOT IN (SELECT [TblCaregiverHospitalREL].[HospitalId] 
									   FROM [TblCaregiverHospitalREL] 
									   WHERE [TblCaregiverHospitalREL].[CaregiverId] = @caregiverId)        


IF (SELECT COUNT(*) FROM [SecurityOauthApplication_Static] WHERE [ApplicationKey] = @ApplicationKey ) = 0
BEGIN
    PRINT 'Create new application keys'
    INSERT [SecurityOauthApplication_Static] ([ApplicationName], [ApplicationKey], [ApplicationSecret], [CaregiverId], [SecurityOauthExpirationId], [CreatedBy_Id]) 
    VALUES(@APIUserName, @ApplicationKey, @ApplicationSecret, @caregiverId, 1, @AdmiUserId)
	SET @oauthAppId = SCOPE_IDENTITY()
END 
ELSE
BEGIN
    SELECT @oauthAppId = SecurityOauthApplicationId FROM [SecurityOauthApplication_Static] WHERE [ApplicationKey] = @ApplicationKey  
END 

IF (SELECT COUNT(*) FROM [SecurityRoleCaregiverREL] WHERE [SecurityRoleId] = @roleId AND [CaregiverId] = @caregiverId ) = 0
BEGIN   
    PRINT 'Map new role to new caregiver'
    INSERT [SecurityRoleCaregiverREL] ([SecurityRoleId], [CaregiverId], [CreatedBy_Id])
    VALUES(@roleId, @caregiverId, @AdmiUserId )           
END         

-------------------------		
--Security Activities----
--3000,3002,3017,3022--
-------------------------
IF (SELECT COUNT(*) FROM [SecurityActivityRoleREL] WHERE [SecurityActivityId] = 3000 AND [SecurityRoleId] = @roleId ) = 0
BEGIN          
    PRINT 'Map PatientDemographicsRead'
    INSERT [SecurityActivityRoleREL] ( [SecurityActivityId], [SecurityRoleId])
    VALUES(3000, @roleId)  -- PatientDemographicsRead
END

IF (SELECT COUNT(*) FROM [SecurityActivityRoleREL] WHERE [SecurityActivityId] = 3002 AND [SecurityRoleId] = @roleId ) = 0
BEGIN          
    PRINT 'Map PatientDemographicsWrite'
    INSERT [SecurityActivityRoleREL] ( [SecurityActivityId], [SecurityRoleId])
    VALUES(3002, @roleId)  -- PatientDemographicsWrite             
END            
        
IF (SELECT COUNT(*) FROM [SecurityActivityRoleREL] WHERE [SecurityActivityId] = 3017 AND [SecurityRoleId] = @roleId ) = 0
BEGIN          
    PRINT 'Map PatientsReadAll'
    INSERT [SecurityActivityRoleREL] ( [SecurityActivityId], [SecurityRoleId])
    VALUES(3017, @roleId)  -- PatientsReadAll             
END        

IF (SELECT COUNT(*) FROM [SecurityActivityRoleREL] WHERE [SecurityActivityId] = 3022 AND [SecurityRoleId] = @roleId ) = 0
BEGIN		
	PRINT 'Map PatientCareplanRead'
	INSERT [SecurityActivityRoleREL] ( [SecurityActivityId], [SecurityRoleId])
	VALUES(3022, @roleId)	-- PatientCareplanRead		
END            

IF (SELECT COUNT(*) FROM [SecurityActivityRoleREL] WHERE [SecurityActivityId] = 3030 AND [SecurityRoleId] = @roleId ) = 0
BEGIN   
    PRINT 'Map PatientResponseApiRead'
    INSERT [SecurityActivityRoleREL] ( [SecurityActivityId], [SecurityRoleId])
    VALUES(3030, @roleId)  -- PatientResponseApiRead
END

IF (SELECT COUNT(*) FROM [SecurityActivityRoleREL] WHERE [SecurityActivityId] = 5000 AND [SecurityRoleId] = @roleId ) = 0
BEGIN		
	PRINT 'Map RPMAndroidApp'
	INSERT [SecurityActivityRoleREL] ( [SecurityActivityId], [SecurityRoleId])
	VALUES(5000, @roleId)	-- RPMAndroidApp		
END

IF (SELECT COUNT(*) FROM [SecurityActivityRoleREL] WHERE [SecurityActivityId] = 5003 AND [SecurityRoleId] = @roleId ) = 0
BEGIN   
    PRINT 'Map RPMClient'
    INSERT [SecurityActivityRoleREL] ( [SecurityActivityId], [SecurityRoleId])
    VALUES(5003, @roleId)  -- RPMClient
END

IF (SELECT COUNT(*) FROM [SecurityActivityRoleREL] WHERE [SecurityActivityId] = 1029 AND [SecurityRoleId] = @roleId ) = 0
BEGIN   
    PRINT 'Map ConfigurationProviderGroupWrite'
    INSERT [SecurityActivityRoleREL] ( [SecurityActivityId], [SecurityRoleId])
    VALUES(1029, @roleId)  -- ConfigurationProviderGroupWrite
END

IF (SELECT COUNT(*) FROM [SecurityActivityRoleREL] WHERE [SecurityActivityId] = 2001 AND [SecurityRoleId] = @roleId ) = 0
BEGIN   
    PRINT 'Map CaregiversWrite'
    INSERT [SecurityActivityRoleREL] ( [SecurityActivityId], [SecurityRoleId])
    VALUES(2001, @roleId)  -- CaregiversWrite
END

IF (SELECT COUNT(*) FROM [SecurityActivityRoleREL] WHERE [SecurityActivityId] = 1039 AND [SecurityRoleId] = @roleId ) = 0
BEGIN   
    PRINT 'Map SecurityVivifyDevelopmentLevelAccessWrite'
    INSERT [SecurityActivityRoleREL] ( [SecurityActivityId], [SecurityRoleId])
    VALUES(1039, @roleId)  -- SecurityVivifyDevelopmentLevelAccessWrite
END

IF (SELECT COUNT(*) FROM [SecurityActivityRoleREL] WHERE [SecurityActivityId] = 3099 AND [SecurityRoleId] = @roleId ) = 0
BEGIN   
    PRINT 'Map PatientHospitalUpdate'
    INSERT [SecurityActivityRoleREL] ( [SecurityActivityId], [SecurityRoleId])
    VALUES(3099, @roleId)  -- PatientHospitalUpdate
END


IF (SELECT COUNT(*) FROM [SecurityActivityRoleREL] WHERE [SecurityActivityId] = 1051 AND [SecurityRoleId] = @roleId ) = 0
BEGIN   
    PRINT 'Map SpecialHandlingReadAndWrite'
    INSERT [SecurityActivityRoleREL] ( [SecurityActivityId], [SecurityRoleId])
    VALUES(1051, @roleId)  -- SpecialHandlingReadAndWrite
END

IF (SELECT COUNT(*) FROM [SecurityActivityRoleREL] WHERE [SecurityActivityId] = 3004 AND [SecurityRoleId] = @roleId ) = 0
BEGIN   
    PRINT 'Map PatientAlertsWrite'
    INSERT [SecurityActivityRoleREL] ( [SecurityActivityId], [SecurityRoleId])
    VALUES(3004, @roleId)  -- PatientAlertsWrite
END

IF (SELECT COUNT(*) FROM [SecurityActivityRoleREL] WHERE [SecurityActivityId] = 3085 AND [SecurityRoleId] = @roleId ) = 0
BEGIN   
    PRINT 'Map PatientNotificationsWrite'
    INSERT [SecurityActivityRoleREL] ( [SecurityActivityId], [SecurityRoleId])
    VALUES(3085, @roleId)  -- PatientNotificationsWrite
END

IF (SELECT COUNT(*) FROM [SecurityActivityRoleREL] WHERE [SecurityActivityId] = 5007 AND [SecurityRoleId] = @roleId ) = 0
BEGIN   
    PRINT 'Map PatientApiReadAll'
    INSERT [SecurityActivityRoleREL] ( [SecurityActivityId], [SecurityRoleId])
    VALUES(5007, @roleId)  -- PatientApiReadAll
END

IF (SELECT COUNT(*) FROM [SecurityActivityRoleREL] WHERE [SecurityActivityId] = 3087 AND [SecurityRoleId] = @roleId ) = 0
BEGIN   
    PRINT 'Map AllHospitalAccess'
    INSERT [SecurityActivityRoleREL] ( [SecurityActivityId], [SecurityRoleId])
    VALUES(3087, @roleId)  -- AllHospitalAccess
END

IF (SELECT COUNT(*) FROM SecurityUserSession WHERE SessionToken = @SessionToken) = 0
BEGIN 
--Hard Session
Print @oauthAppId
INSERT INTO SecurityUserSession(SessionToken, CaregiverId, ClientId, CreatedDateTime_UTC, CreatedBy_Id, ExpiresDateTime_UTC, SiteId, SecurityOauthApplicationId) VALUES
(@SessionToken, @caregiverId, '', GETUTCDATE(), 2, '9999-12-31 23:59:59.997', @tokenSiteId, @oauthAppId)
END
  