IF OBJECT_ID('[NewPatientResponses]') IS NULL
BEGIN
 
 
CREATE TABLE [dbo].[NewPatientResponses](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Patient] [nvarchar](max) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,	
	[Error] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NULL,
 CONSTRAINT [PK_NewPatientResponses] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
 
 
END
GO
 
	 