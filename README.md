
# Import Patients #
This is a console application.

# What is this repository for? #
 To create new patients from a excel file. The app makes rest api call to "api\patient" for each patient in the file. 


# configurations #
RestSharp nuget package


App.config 
	portalUrl : add portalurl to make a post to "api\patient"
	ApiSessionPortal: Session token to authorize the api call . Run the scripts in database folder under this project to make the long live token is created in the customer db to upload patients.  
	connectionstring: sql connection to log the responses for each patient.
	sleep: after every API call the main thread sleep is called to avoid any thread related issues.
	break: After the number of configured records, application pauses to confirm if you wish to continue.
	

#Patient upload file requirements #
Add new property , make changes to patient object in Upload.cs and add the property to DTO.Patient class (Mapping in CTP Models\Api\PatientApiModel).  
Only Lastname is mandatory. 
Parameters application reads from is below. 
	FirstName 
	LastName 
	MiddleName  
	Gender  
	DOB  
	Address1  
	Address2 
	State 
	City 
	ZipCode 
	Email 
	Phone1 
	Phone2
	ServiceLevelId (servicelevel table)
	TimeZoneId  
	HospitalId 
	PopulationId 
	PopulationGroupId
	MRN
	Payerid
	ReferralId
	LanguageId
	CustomField   <!--ex: StudyId-->
	ProviderId    <!--ProviderMasterId from provider table is needed to update provider on the patient-->
	ProviderGroupId   


# Monitoring status Change #
	status
	paientid
	reason 
	reasontext


# Update MRN #
	patientid
	oldmrn
	newmrn

# "Provider upload  # , # "Provider read  #

	firstname
	lastname
	middleinitial
	license
	specialityid
	titleid
	address1
	address2
	State 
	City 
	ZipCode
	Phone1 
	Phone2
	mobilenumber
	faxnumber
	email
	providergroupid

# "CTM upload  # # "CTM read  #
	username	
	firstname
	lastname
	gender
	authorizationmode
	email
	timezoneid
	phone
	password
	caregivertitleid
	hospitalid
	securityrole
	sites

# Service Level assign +GO  #
patientid
phone
servicelevel

# "Unassign Service Level +GO  #
patientid
	  
# log #

Responses saved in the table [vivifyplatform].[dbo].[NewPatientResponses]
You can point app.config  connectionstring to localhost for UHG when importing data as we dont have the permission to write .
Columns:
	Patient:  Line fetched from excel file
	LastName: Second parameter of the line
	Error:    If null then patient created else the error

# How to run the application #

1.Create the log table NewPatientResponses in client sql server. 
2.Create systemupload user and sessiontoken using the scripts in database folder. Make sure the user and sessiontoken are created. This is one time script.
3.Use an excel file to upload. Make sure the file has the exact column names. 
4.Create a fresh file instead of the one in the ticket to avoid any permission issues. As the file we get is password protected.
5.Connect to the database and get below id columns and update the file:
	ServiceLevel		<!-- ServiceLevel -->
	TimeZoneId			<!-- TimeZoneMappingEnum -->
	HospitalId			<!-- TblHospitalMaster -->
	PopulationId		<!-- TblPopulation -->
	PopulationGroupId   <!-- PopulationGroupEnum -->
	Payerid				<!-- PayersEnum -->
	ReferralId			<!-- ReferralEnum -->
	LanguageId			<!-- PatientLanguage_Static -->
	ProviderId is the Providermasterid from provider table
	ProviderGroupId

6.Change the settings in web.config ex: portalurl, connection string
7.  
	ex:  <appSettings>
			<add key="ApiSessionPortal" value="E26038FC-F885-41CD-92BC-BF7C49C6F0B9"/>		<!--Default site or Site id 1. The session token will determine the siteid -->
			<!--<add key="ApiSessionPortal" value="E26038FC-F885-41CD-92BC-BF7C49C6F0B2"/>-->		<!--Site id 2-->
			<add key="portalUrl" value="http://localhost"/>
			<add key="connectionstring" value="Server=localhost;Database=vivifyplatform;user id=devuser;password=Health1"/><!-- Use localhost conn if no db access ex: mcp-->
			<add key="sleep" value="1" />
			<add key="batchSize" value="10" />  <!--for parallel posting-->
		</appSettings> 

		
9. Different site use separate token.
	 
 
10.Open command line, have the path of the application executable and path of the excel file  and run below command.
	
	ex: >ImportPatients file.xlsx

	Then the prompt will ask for an option. 
	Current options :
				 {"c","Check phone and dob are present and validate" }
                ,{"n","Read the patient upload data to verify the code is pulling up all fields" }
                ,{"p","upload patients to portal" }
                ,{"e","verify if patient already exists" }
                ,{"status" ,"Monitoring status Change"}
                ,{"mrn", "Update MRN" }
                ,{"pu" , "Provider upload"}
                ,{"pr" , "Provider read"}
                ,{"cu" , "CTM upload"}
                ,{"cr" , "CTM read"}
                ,{"sl", "Service Level assign +GO" }
                ,{"nosl", "Unassign Service Level +GO" } 

	 
