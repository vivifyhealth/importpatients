﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;

namespace ImportPatients
{
    class UploadProviders
    {
        public UploadProviders()
        {
        }


        public void PostProvider(string file, bool createProvider)
        {
         
            DataTable data = Common.GetData(file, true);
            List<DataTable> tables = UploadPatients.SplitTable(data, Convert.ToInt32(ConfigurationManager.AppSettings["batchSize"]));
            try
            {

                foreach (DataTable dt in tables)
                {
                    Parallel.ForEach(dt.AsEnumerable(), dr =>
                    {
                        var provider = new DTO.ProviderModel();
                        string rowData = "";
                        int id = DataTypes.GetIntData(dr["autoID"]);

                        try
                        {
                            GetProviderData(provider, dr);
                            provider.IsEnabled = true;

                            if (!String.IsNullOrEmpty(provider.LastName))
                            {
                                rowData = "RowNumber:" + id + " ," + provider.FirstName + "," + provider.LastName + "," + provider.MiddleInitial + ","  
                                            + provider.License + " ," + provider.CaregiverTitleId + " ," + provider.CaregiverSpecialtyId + " ,"
                                            + provider.AddressOne + " ," + provider.City + " ," + provider.State + " ," + provider.ZipCode + " ,"
                                            + provider.PhoneNumber1 + " ," + provider.PhoneNumber2 + " ," + provider.FaxNumber + " ," + provider.EmailAddress + " ,"
                                            + provider.ProviderGroupId;
                                Logger.Write(rowData);
                                if (createProvider)
                                {
                                    VivifyRestClient client = new VivifyRestClient();
                                    client.CallProviderApi(provider, rowData);
                                }
                            }

                        }
                        catch (HttpResponseException unauthex)
                        {
                            Logger.Write("Error " + unauthex.Message);
                            tables.Clear();
                            dt.Clear();

                        }
                        catch (Exception ex)
                        {
                            Logger.Write("Error " + ex.Message);
                            Logger.Detail(ex.StackTrace);
                        }
                    });

                    Logger.Write("Do you wish to continue? Press X to exit.");
                    while (Console.ReadKey(true).Key == ConsoleKey.X)
                    {
                        Common.WriteToLog();
                        Environment.Exit(0);
                    }
                }
                Logger.Write("Upload Complete.");
            }
            catch (Exception ex)
            {
                Logger.Write("Error: " + ex.Message);
                Logger.Detail(ex.StackTrace);
            }
        }


        private void GetProviderData(DTO.ProviderModel provider, DataRow dr)
        {

            foreach (DataColumn item in dr.Table.Columns)
            {
                switch (item.ColumnName.ToLower())
                {
                    case "firstname":
                        {
                            provider.FirstName = DataTypes.GetStringData(dr[item]);

                        }
                        break;
                    case "lastname":
                        {
                            provider.LastName = DataTypes.GetStringData(dr[item]);
                        }
                        break;
                    case "middleinitial":
                    case "mi":
                        {
                            provider.MiddleInitial = DataTypes.GetStringData(dr[item]);
                        }
                        break;
                    case "license":
                        {
                            provider.License = DataTypes.GetStringData(dr[item]);
                        }
                        break;
                    case "specialityid":
                        {
                            provider.CaregiverSpecialtyId = DataTypes.GetIntDataWithNull(dr[item]);
                        }
                        break;
                    case "titleid":
                        {
                            provider.CaregiverTitleId = DataTypes.GetIntDataWithNull(dr[item]);
                        }
                        break;
                    case "addressone":
                    case "address1":
                        {
                            provider.AddressOne = DataTypes.GetStringData(dr[item]);
                        }
                        break;
                    case "addresstwo":
                    case "address2":
                        {
                            provider.AddressTwo = DataTypes.GetStringData(dr[item]);
                        }
                        break;
                    case "city":
                        {
                            provider.City = DataTypes.GetStringData(dr[item]);
                        }
                        break;
                    case "state":
                        {
                            provider.State = DataTypes.GetStringData(dr[item]);
                        }
                        break;
                    case "zipcode":
                    case "zip":
                        {
                            provider.ZipCode = DataTypes.GetStringData(dr[item]);
                            if (provider.ZipCode.EndsWith("-"))
                            {
                                provider.ZipCode = provider.ZipCode.Replace("-", "");
                            }
                        }
                        break;
                    case "primaryphone":
                    case "phone1":
                        {
                            provider.PhoneNumber1 = String.IsNullOrEmpty(dr[item].ToString()) ? "" : Regex.Replace(dr[item].ToString(), "[^0-9]", "");
                            if (provider.PhoneNumber1.Length == 1)
                            {
                                provider.PhoneNumber1 = "";
                            }
                            else if (provider.PhoneNumber1.Length == 10)
                            {
                                provider.PhoneNumber1 = "1" + provider.PhoneNumber1;
                            }
                        }
                        break;
                    case "secondaryphone":
                    case "phone2":
                        {
                            provider.PhoneNumber2 = String.IsNullOrEmpty(dr[item].ToString()) ? "" : Regex.Replace(dr[item].ToString(), "[^0-9]", "");
                            if (provider.PhoneNumber2.Length == 1)
                            {
                                provider.PhoneNumber2 = "";
                            }
                            else if (provider.PhoneNumber2.Length == 10)
                            {
                                provider.PhoneNumber2 = "1" + provider.PhoneNumber2;
                            }
                        }
                        break;
                    case "mobilephone":
                    case "mobilenumber":
                        {
                            provider.MobileNumber = String.IsNullOrEmpty(dr[item].ToString()) ? "" : Regex.Replace(dr[item].ToString(), "[^0-9]", "");
                            if (provider.MobileNumber.Length == 1)
                            {
                                provider.MobileNumber = "";
                            }
                            else if (provider.MobileNumber.Length == 10)
                            {
                                provider.MobileNumber = "1" + provider.MobileNumber;
                            }
                        }
                        break;
                    case "faxnumber":
                    case "fax":
                        {
                            provider.FaxNumber = String.IsNullOrEmpty(dr[item].ToString()) ? "" : Regex.Replace(dr[item].ToString(), "[^0-9]", "");
                            if (provider.FaxNumber.Length == 1)
                            {
                                provider.FaxNumber = "";
                            }
                            else if (provider.FaxNumber.Length == 10)
                            {
                                provider.FaxNumber = "1" + provider.FaxNumber;
                            }
                        }
                        break;
                    case "email":
                        {
                            provider.EmailAddress = DataTypes.GetStringData(dr[item]);
                        }
                        break;
                    case "providergroupid":
                        {
                            provider.ProviderGroupId = DataTypes.GetIntDataWithNull(dr[item]);
                          
                        }
                        break;

                    default:
                        break;
                };
            }

        }

    }
}
