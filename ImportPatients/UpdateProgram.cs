﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;

namespace ImportPatients
{
    class UpdateProgram
    {
        public UpdateProgram()
        {
        }


        public void PostNewProgram(string file, bool create)
        {

            DataTable data = Common.GetData(file, true);
            List<DataTable> tables = UploadPatients.SplitTable(data, Convert.ToInt32(ConfigurationManager.AppSettings["batchSize"]));
            try
            {

                foreach (DataTable dt in tables)
                {
                    Parallel.ForEach(dt.AsEnumerable(), dr =>
                    {
                        var program = new DTO.PatientProgram();
                        string rowData = "";
                        int id = DataTypes.GetIntData(dr["autoID"]);

                        try
                        {
                            GetProgramData(program, dr);

                            if (program.PopulationId > 0)
                            {
                                rowData = "RowNumber:" + id + " ," + program.Id + "," + program.PopulationId + "," + program.PopulationGroupId
                                + "," + program.ProgramStartDate + "," + program.ProgramDurationId + "," + program.PatientMasterId;
                                
                                Logger.Write(rowData);
                                if (create)
                                {
                                    VivifyRestClient client = new VivifyRestClient();
                                    client.CallPatientProgramApi(program,rowData);
                                }
                            }

                        }
                        catch (HttpResponseException unauthex)
                        {
                            Logger.Write("Error " + unauthex.Message);
                            tables.Clear();
                            dt.Clear();

                        }
                        catch (Exception ex)
                        {
                            Logger.Write("Error " + ex.Message);
                            Logger.Detail(ex.StackTrace);
                        }
                    });

                    Logger.Write("Do you wish to continue? Press X to exit.");
                    while (Console.ReadKey(true).Key == ConsoleKey.X)
                    {
                        Common.WriteToLog();
                        Environment.Exit(0);
                    }
                }
                Logger.Write("Upload Complete.");
            }
            catch (Exception ex)
            {
                Logger.Write("Error: " + ex.Message);
                Logger.Detail(ex.StackTrace);
            }
        }
         

        private void GetProgramData(DTO.PatientProgram pg, DataRow dr)
        {

            foreach (DataColumn item in dr.Table.Columns)
            {
                switch (item.ColumnName.ToLower())
                {
                    case "patientid":
                        {
                            pg.Id = (int)DataTypes.GetIntDataWithNull(dr[item]);

                        }
                        break;
                    case "populationid":
                    case "programid":
                        {
                            pg.PopulationId = (int)DataTypes.GetIntDataWithNull(dr[item]);

                        }
                        break;
                    case "populationgroupid":
                    case "programgroupid":
                        {
                            pg.PopulationGroupId = (int)DataTypes.GetIntDataWithNull(dr[item]);
                        }
                        break;
                   case "programdurationid":
                        {
                            pg.ProgramDurationId = (int)DataTypes.GetIntDataWithNull(dr[item]);
                        }
                        break;
                    case "programstartdate":
                        {
                            string startdate = dr[item].ToString().Trim();
                            if (!String.IsNullOrEmpty(startdate))
                            {                                
                                pg.ProgramStartDate = DateTime.Parse(startdate);
                            }                             
                        }
                        break;

                    default:
                        break;
                };
            }

        }
    }
}
