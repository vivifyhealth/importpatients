﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ImportPatients
{
    public class Common
    {
        public Common()
        {
        }
        public static bool WriteToLog()
        {
            using (StreamWriter outputFile = new StreamWriter(Path.Combine(Environment.CurrentDirectory, "log.txt"), true))
            {
                outputFile.Write(Logger.LogString);
            }

            if (Logger.ErrorLogString.Length > 0)
            {
                using (StreamWriter outputFile = new StreamWriter(Path.Combine(Environment.CurrentDirectory, "Errorlog.txt"), true))
                {
                    outputFile.Write(Logger.ErrorLogString);
                }
            }

            return true;

        }
        public string GetSiteName(string token)
        {
            if (token.EndsWith("B9"))
            {
                token = " patients to SITE 1";
            }
            else if (token.EndsWith("B2"))
            {
                token = " patients to SITE 2";
            }
            else if (token.EndsWith("B3"))
            {
                token = " patients to SITE 3";
            }

            return token;
        }

        public static string GetGender(string gender)
        {
            string result = "";
            if (gender.ToUpper().StartsWith("M"))
            {
                result = "Male";
            }
            else if (gender.ToUpper().StartsWith("F"))
            {
                result = "Female";
            }
            return result;
        }

        public static string GetPhone(object item)
        {
            string result = "";
            result = String.IsNullOrEmpty(item.ToString())
                             ? ""
                             : Regex.Replace(item.ToString(), "[^0-9]", "");
            if (result.Length == 1)
            {
                result = "";
            }
            else if (result.Length == 10)
            {
                result = "1" + result;
            }
            return result;
        }

        public static DataTable GetData(string file, bool isExcelReader)
        {
            System.Data.DataTable dt = new DataTable();
            if (isExcelReader)
            {
                using (var reader = new ExcelDataReader(file))
                {
                    dt.Columns.Add(new DataColumn("autoID") { AutoIncrement = true, AutoIncrementSeed = 1 });
                    dt.Load(reader);
                }
            }
            else
            {
                string connString = Conn.GetConnString(file);
                dt = GetDataTable("SELECT * from [Sheet1$]", connString);
            }

            return dt;
        }


        public static DataTable GetDataTable(string selectcmd, string connectionString)
        {
            System.Data.DataTable dt = new DataTable();
            using (OleDbConnection conn = new OleDbConnection(connectionString))
            {
                conn.Open();
                using (OleDbCommand cmd = new OleDbCommand(selectcmd, conn))
                {
                    using (OleDbDataReader rdr = cmd.ExecuteReader())
                    {
                        dt.Columns.Add(new DataColumn("autoID") { AutoIncrement = true, AutoIncrementSeed = 1 });
                        dt.Load(rdr);

                        return dt;
                    }
                }
            }
        }
 
          

    }
}
