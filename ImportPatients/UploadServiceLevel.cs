﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;

namespace ImportPatients
{
    class UploadServiceLevel
    {
        public UploadServiceLevel()
        {
        }

        public void PostServiceLevel(string file, bool createSL)
        {

            DataTable data = Common.GetData(file, true);
            List<DataTable> tables = UploadPatients.SplitTable(data, Convert.ToInt32(ConfigurationManager.AppSettings["batchSize"]));
            try
            {

                foreach (DataTable dt in tables)
                {
                    Parallel.ForEach(dt.AsEnumerable(), dr =>
                    {
                        var serviceLevel = new DTO.PatientServiceLevel();
                        string rowData = "";
                        int id = DataTypes.GetIntData(dr["autoID"]);

                        try
                        {
                            GetServiceLevelData(serviceLevel, dr);

                            if (serviceLevel.ServiceLevelId > 0)
                            {
                                rowData = "RowNumber:" + id + " ,";
                                Logger.Write(rowData);
                                if (createSL)
                                {
                                    VivifyRestClient client = new VivifyRestClient();
                                    client.CallPatientServicelevelApi(serviceLevel);
                                }
                            }

                        }
                        catch (HttpResponseException unauthex)
                        {
                            Logger.Write("Error " + unauthex.Message);
                            tables.Clear();
                            dt.Clear();

                        }
                        catch (Exception ex)
                        {
                            Logger.Write("Error " + ex.Message);
                            Logger.Detail(ex.StackTrace);
                        }
                    });

                    Logger.Write("Do you wish to continue? Press X to exit.");
                    while (Console.ReadKey(true).Key == ConsoleKey.X)
                    {
                        Common.WriteToLog();
                        Environment.Exit(0);
                    }
                }
                Logger.Write("Upload Complete.");
            }
            catch (Exception ex)
            {
                Logger.Write("Error: " + ex.Message);
                Logger.Detail(ex.StackTrace);
            }
        }

        public void UnassignServiceLevel(string file)
        {

            DataTable data = Common.GetData(file, true);
            List<DataTable> tables = UploadPatients.SplitTable(data, Convert.ToInt32(ConfigurationManager.AppSettings["batchSize"]));
            try
            {

                foreach (DataTable dt in tables)
                {
                    Parallel.ForEach(dt.AsEnumerable(), dr =>
                    {
                        var serviceLevel = new DTO.PatientServiceLevel();
                        string rowData = "";
                        int id = DataTypes.GetIntData(dr["autoID"]);

                        try
                        {
                            GetServiceLevelData(serviceLevel, dr);

                            if (serviceLevel.PatientId > 0)
                            {
                                rowData = "RowNumber:" + id + " ,";
                                Logger.Write(rowData);

                                VivifyRestClient client = new VivifyRestClient();
                                client.UnassignServiceLevel(serviceLevel.PatientId);

                            }

                        }
                        catch (HttpResponseException unauthex)
                        {
                            Logger.Write("Error " + unauthex.Message);
                            tables.Clear();
                            dt.Clear();

                        }
                        catch (Exception ex)
                        {
                            Logger.Write("Error " + ex.Message);
                            Logger.Detail(ex.StackTrace);
                        }
                    });

                    Logger.Write("Do you wish to continue? Press X to exit.");
                    while (Console.ReadKey(true).Key == ConsoleKey.X)
                    {
                        Common.WriteToLog();
                        Environment.Exit(0);
                    }
                }
                Logger.Write("Upload Complete.");
            }
            catch (Exception ex)
            {
                Logger.Write("Error: " + ex.Message);
                Logger.Detail(ex.StackTrace);
            }
        }


        private void GetServiceLevelData(DTO.PatientServiceLevel sl, DataRow dr)
        {

            foreach (DataColumn item in dr.Table.Columns)
            {
                switch (item.ColumnName.ToLower())
                {
                    case "patientid":
                        {
                            sl.PatientId = (int)DataTypes.GetIntDataWithNull(dr[item]);

                        }
                        break;
                    case "servicelevelid":
                        {
                            sl.ServiceLevelId = (int)DataTypes.GetIntDataWithNull(dr[item]);

                        }
                        break;
                    case "phone":
                        {
                            sl.PhoneNum = DataTypes.GetStringData(dr[item]);
                        }
                        break;
                    case "pin":
                        {
                            sl.PIN = DataTypes.GetStringData(dr[item]);
                        }
                        break;

                    default:
                        break;
                };
            }

        }
    }
}
