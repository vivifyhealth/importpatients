﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportPatients
{
    public static class Logger
    {
        public static StringBuilder LogString = new StringBuilder();
        public static StringBuilder ErrorLogString = new StringBuilder();
        public static void Write(string str)
        {
            Console.WriteLine(str);
            LogString.Append(str).Append(Environment.NewLine);
        }

        public static void Detail(string str)
        {
            LogString.Append(str).Append(Environment.NewLine);
        }
        public static void ErrorLog(string str)
        {
            ErrorLogString.Append(str).Append(Environment.NewLine);
        }

    }
}
