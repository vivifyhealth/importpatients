﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportPatients
{
    internal static class Conn
    {
        public static string GetConnString(string file)
        {
            string connString =
                    @"Provider=Microsoft.ACE.OLEDB.12.0;" +
                    @"Data Source=" + file + ";" +
                    @"Extended Properties=Excel 12.0;";

            return connString;
        }
    }
}
