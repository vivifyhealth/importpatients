﻿using System;
using System.Collections.Generic;
using System.Linq; 
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace ImportPatients.DTO
{
    class PatientSpecialHandling
    {
        public int PatientSpecialHandlingId { get; set; }

        [MinValue(1)]
        public int PatientId { get; set; }

        [MinValue(1)]
        public int SpecialHandlingId { get; set; }

        public DateTime CreatedDateTime_UTC { get; set; }
        public int CreatedBy_Id { get; set; }
        public DateTime? DeletedDateTime_UTC { get; set; }
        public int? DeletedBy_Id { get; set; }

        public SpecialHandlingModel SpecialHandlingModel { get; set; }
    }
    public class SpecialHandlingModel
    {
        public int SpecialHandlingId { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        public string Icon { get; set; }
        public string Tags { get; set; }
        public int SortOrder { get; set; }
        public bool IsDefault { get; set; }
        public bool IsEnabled { get; set; }
        public int PatientCount { get; set; }
        public DateTime CreatedDateTime_UTC { get; set; }
        public int CreatedBy_Id { get; set; }
        public DateTime? LastModifiedDateTime_UTC { get; set; }
        public int? LastModifiedBy_Id { get; set; }
        public int SiteId { get; set; }
    }
    public class MinValue : ValidationAttribute
    {
        private readonly int _minValue;

        public MinValue(int minValue)
        {
            _minValue = minValue;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            int val = Convert.ToInt32(value);
            if (val < _minValue)
            {
                string message = string.Format("{0} must be greater than {1}", validationContext.DisplayName, _minValue);
                return new ValidationResult(message);
            }

            return ValidationResult.Success;
        }
    }

     
}
