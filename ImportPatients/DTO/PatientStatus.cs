﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportPatients.DTO
{
//CreatedBy: 1094
//CreatedDtUTC: "2020-09-21T16:00:38Z"
//DeletedBy_Id: null
//DeletedDateTime_UTC: null
//Id: 5038
//IsActivePatient: false
//IsFinalState: false
//MostRecentRecord: false
//OriginalMonitoringStatusId: 0
//PatientId: 4035
//ReasonText: "testing"
//StatusChangeReasonId: null
//StatusName: "Inactive"
//Statusid: 2
    class PatientStatus
    {
        public int Id { get; set; }
        public int PatientId { get; set; }
        public MonitoringStatusEnum Statusid { get; set; }
        public int OriginalMonitoringStatusId { get; set; }
        public bool MostRecentRecord { get; set; }
        public bool IsActivePatient { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDtUTC { get; set; }
        public DateTime? DeletedDateTime_UTC { get; set; }
        public int? DeletedBy_Id { get; set; }
        public int? StatusChangeReasonId { get; set; }
        public string ReasonText { get; set; }
        public string StatusName { get; set; }
        public bool IsFinalState { get; set; }
    }
    public enum MonitoringStatusEnum
    {
        None = 0,
        Active = 1,
        Inactive = 2,
        Screening = 3,
        Completed = 4,
        StandardOfCare = 5,
        ScreenFail = 6,
        Deleted = 7,
        PreActive = 8,
        Invited = 9
    }

    
}
