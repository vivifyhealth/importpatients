﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportPatients.DTO
{
    class Patient
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Middle_Name { get; set; }
        public string Gender { get; set; }
        public Nullable<DateTime> DOB { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string Email { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public int ServiceLevelId { get; set; }
        public int ServiceLevelTypeId { get; set; }
        public int TimeZoneId { get; set; }
        public int HospitalId { get; set; }
        public Nullable<int> PopulationId { get; set; }
        public Nullable<int> PopulationGroupId { get; set; }
        public string MRNIdent { get; set; }
        public ProviderModel Provider { get; set; }
        public int? PayerId { get; set; }
        public int? ReferralId { get; set; }
        public int LanguageId { get; set; }
        public string CustomField { get; set; }//studyid
        public int? ProviderGroupId { get; set; }
        public Nullable<int> RaceId { get; set; }
        public ICollection<PatientCaregiverModel> PatientCaregiverList { get; set; }
    }

    public class PatientCaregiverModel
    {
        public int Id { get; set; }
        public string HL7Ident { get; set; }

        [MinValue(1)]
        public int PatientId { get; set; }

        [MinValue(1)]
        public int CaregiverId { get; set; }

        public string DisplayName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsDeleted { get; set; }
        public int CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime CreatedDtUTC { get; set; }
        public DateTime? ModifiedDtUTC { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Phone2 { get; set; }
    }

    public class ProviderModel
    {
        public int ProviderId { get; set; }
        public int ProviderMasterId { get; set; }
        public DateTime CreatedDateTime_UTC { get; set; }
        public int CreatedBy_Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OtherName { get; set; }
        public string FullName => $"{FirstName} {LastName}";
        public string MiddleInitial { get; set; }
        public string PhoneNumber1 { get; set; }
        public string PhoneNumber2 { get; set; }
        public string MobileNumber { get; set; }
        public string AnsweringServiceNumber { get; set; }
        public string FaxNumber { get; set; }
        public string EmailAddress { get; set; }
        public string AddressOne { get; set; }
        public string AddressTwo { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Hl7Ident { get; set; }
        public string License { get; set; }
        public bool FreeText { get; set; }
        public int? CaregiverTitleId { get; set; }
        public int? CaregiverSpecialtyId { get; set; }

        public bool IsEnabled { get; set; }
        public int? ProviderGroupId { get; set; }
        public CaregiverTitleModel CaregiverTitle { get; set; }
        public CaregiverSpecialtyModel CaregiverSpecialty { get; set; }
        public ProviderGroupModel ProviderGroup { get; set; }

    }

    public class CaregiverSpecialtyModel
    {
        public int SpecialtyId { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public bool IsDefault { get; set; }
        public bool IsEnabled { get; set; }
    }

    public class CaregiverTitleModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public bool IsDefault { get; set; }
        public bool IsClinical { get; set; }
    }

    public class ProviderGroupModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public bool IsDefault { get; set; }
        public bool IsEnabled { get; set; }
    }

    public class ServiceLevelModel
    {
        public int PatientId { get; set; }

        public PatientServiceLevelModel servicelevelmodel { get; set; }
    }

    public class PatientServiceLevelModel
    {
        public int PatientServiceLevelId { get; set; }
        public int PatientId { get; set; }
        public int ServiceLevelId { get; set; } // required to update service level
        public int? KitId { get; set; }
        public string PhoneNum { get; set; }
    }
}
