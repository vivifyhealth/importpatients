﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportPatients.DTO
{
    class PatientServiceLevel
    {
         
            public int PatientServiceLevelId { get; set; }
            public int PatientId { get; set; }
            public int ServiceLevelId { get; set; }
            public int? KitId { get; set; }
            public DateTime CreatedDateTime_UTC { get; set; }
            public int CreatedBy_Id { get; set; }
            public bool TabletRequired { get; set; }
            public bool BillableByStatus { get; set; }
            public string KitNo { get; set; }
            public int? KitCycleId { get; set; }
            public DateTime? DeletedDateTime_UTC { get; set; }           
            public string PhoneNum { get; set; }
            public string PIN { get; set; }
            public string Email { get; set; }
            public string IvrWelcomeDate { get; set; }
            public string IvrWelcomeTime { get; set; }
            public string ByodDate { get; set; }
            public string ByodTime { get; set; }
            public int? GoPlatformId { get; set; }
            public string GoPlatform { get; set; }
             

         
    }
}
