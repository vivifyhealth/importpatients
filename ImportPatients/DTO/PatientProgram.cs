﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportPatients.DTO
{
    class PatientProgram
    {

        public int Id { get; set; }
        public int PatientMasterId { get; set; }
        public int? PopulationGroupId { get; set; }
        public int? PopulationId { get; set; }
        public int? ProgramDurationId { get; set; }
        public DateTime? ProgramStartDate { get; set; }

    }
}
