﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportPatients.DTO
{
    class PatientHospital
    {
        
            public int Id { get; set; }
            public int PatientMasterId { get; set; }
            public int HospitalId { get; set; }
            public string MRNIdent { get; set; }
            public string ConsultNumber { get; set; }
            public string HL7Custom1 { get; set; }
            public string NewPatientIdent { get; set; }
            public string EmpiIdent { get; set; }
            public string CustomField { get; set; }
            public int TimeZoneId { get; set; }
            public int? ReferralId { get; set; }
            public int? PayerId { get; set; }
            public bool IsThirdPartyMonitored { get; set; }
            // these have been added to the Patient Program dialog in the portal
            public IEnumerable<PatientIdentifierApiModel> PatientIdentifiers { get; set; }
            public IEnumerable<CareManagementTypeModel> PatientCareManagementTypes { get; set; }
         
    }
    public class PatientIdentifierApiModel
    {
        public int PatientMasterId { get; set; }
        public int PatientIdentifierEnumId { get; set; }
        public string PatientIdentifierType { get; set; }
        public string Value { get; set; }
        public string Label { get; set; }
    }
    public class CareManagementTypeModel
    {
        public int Id { get; set; }
        public string Acronym { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public bool IsClinicalTimeMeasured { get; set; }
        public bool IsEnabled { get; set; }
    }
}
