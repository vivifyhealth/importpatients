﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportPatients.DTO
{
    class MRN
    {
        public int PatientId { get; set; }
        public string OldMRN { get; set; }
        public string NewMRN { get; set; }
    }
}
