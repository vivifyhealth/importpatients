﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportPatients.DTO
{
    public class Caregiver
    {
        public Caregiver()
        {
            this.SecurityRoles = new HashSet<SecurityRoleModel>();
            this.CaregiverHospitals = new HashSet<CaregiverHospitalModel>();
            this.CaregiverSites = new HashSet<CaregiverSiteModel>();
        }

        public int Id { get; set; }
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string Last_Name { get; set; }
        public string DisplayName
        {
            get
            {
                return string.Format("{0}, {1} {2}", Last_Name, First_Name, CaregiverTitle != null && IsClinical == true ? CaregiverTitle.Name : "");
            }
        }
        public string Gender { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Email { get; set; }
        
        public string Phone1 { get; set; }
       
        public string Phone2 { get; set; }
        public string User_Name { get; set; }
        public string Password { get; set; }
        public bool IsDeleted { get; set; }
        public bool ReceiveEmail { get; set; }
        public int CreatedById { get; set; }
        public CaregiverPickListModel CreatedBy { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public int TimeZoneId { get; set; }
        public string TimeZoneName { get; set; }
        public Nullable<int> CaregiverTitleId { get; set; }
        public bool ExpirePasswordNow { get; set; }
        public int FailedLoginAttempts { get; set; }
        public byte[] Photo { get; set; }
        public string Password1 { get; set; }
        public string Password2 { get; set; }
        public string Password3 { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public Nullable<System.DateTime> LastAgreement { get; set; }
        public List<System.DateTime> LastLogins { get; set; }
        public Nullable<System.DateTime> LastLoginTimeUTC { get; set; }
        public Nullable<System.DateTime> LastFailedLoginTimeUTC { get; set; }
        public Nullable<System.DateTime> PasswordLastResetUTC { get; set; }
        public Nullable<System.DateTime> ModifiedDtUTC { get; set; }
        public DateTime? CreatedDtUTC { get; set; }
        public DateTime? LastEnabledDtUTC { get; set; }
        public DateTime? LastDisabledDtUTC { get; set; }
        public bool IsPasswordInNewAlgo { get; set; }
        public Nullable<int> SpecialtyId { get; set; }
        public string SpecialtyName { get; set; }
        public Nullable<bool> IsSystemUser { get; set; }
        public Nullable<int> AuthorizationModeId { get; set; }
        public string AuthorizationModeName { get; set; }
        public bool AuthorizationModeIsExternal { get; set; }
        public bool IsShownInTimeLog { get; set; }
        public bool IsClinical { get; set; }
        public bool IsThirdParty { get; set; }
        public string HL7Ident { get; set; }
        public string HL7FirstName { get; set; }
        public string HL7LastName { get; set; }
        public bool IsLocked { get; set; }
        public bool IsUnlocked { get; set; }
        public bool ShowUnlockButton { get; set; }
        public bool ReceiveLogisticsMessages { get; set; }
        public int? SiteId { get; set; }
        public int TwoFactorTypeId { get; set; }
        public string TwoFactorCode { get; set; }
        public string TwoFactorSecret { get; set; }
        public bool TwoFactorRequired { get; set; }

        public virtual CaregiverTitleModel CaregiverTitle { get; set; }
        public virtual ICollection<SecurityRoleModel> SecurityRoles { get; set; }
        public virtual ICollection<CaregiverHospitalModel> CaregiverHospitals { get; set; }
        public virtual ICollection<CaregiverSiteModel> CaregiverSites { get; set; }
        
        public bool? CreateUniversityUser { get; set; } = false;
        public string VivifyUniUsername { get; set; }
        public DateTime? VivifyUniCreateDateUTC { get; set; }
        public DateTime? VivifyUniSuspendDateUTC { get; set; }

    }

    public class CaregiverPickListModel
    {
        public int Id { get; set; }
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string Last_Name { get; set; }
        public string DisplayName
        {
            get
            {
                return string.Format("{0}, {1} {2}", Last_Name, First_Name, IsClinical == true ? Title : "");
            }
        }
        public bool IsClinical { get; set; }
        public bool IsThirdParty { get; set; }

        public string Title { get; set; }
    }

    public class SecurityRoleModel
    {
        public SecurityRoleModel()
        {
            this.SecurityActivity = new HashSet<SecurityActivityModel>();
        }
        public int SecurityRoleId { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsReserved { get; set; }
        public int SortOrder { get; set; }
        public string Description { get; set; }
        public bool IsAPIOnlyRole { get; set; }
        public int SecurityActivityId { get; set; }
        public virtual ICollection<SecurityActivityModel> SecurityActivity { get; set; }
    }

    public class SecurityActivityModel
    {
        public int SecurityActivityId { get; set; }
        public string Name { get; set; }
        public int FilterSecurityActivityId { get; set; }
        public string Group { get; set; }
    }

    public class CaregiverHospitalModel
    {
        public int Id { get; set; }
        public bool IsAssigned { get; set; }
        public string FacilityName { get; set; }
        public string FacilityId { get; set; }
        public bool IsDefault { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsThirdParty { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<int> TimeZoneId { get; set; }
    }

    public class CaregiverSiteModel
    {
        public int SiteId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string BillingAccountNumber { get; set; }
        public int SortOrder { get; set; }
        public bool IsAssigned { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsEnabled { get { return true; } }
        public bool IsCurrent { get; set; }  // true for the site the current user is logged into.
    }
}
