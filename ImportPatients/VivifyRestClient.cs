﻿using System;
using System.Configuration;
using RestSharp;
using System.Net;
using System.Data.SqlClient;
using System.Data;
using System.Web.Http;
using System.Data.OleDb;


namespace ImportPatients
{
    class VivifyRestClient
    {
        private readonly RestClient _client;
        private readonly string _url = ConfigurationManager.AppSettings["portalUrl"];
        private readonly string _connectionString = ConfigurationManager.AppSettings["connectionString"];

        public VivifyRestClient()
        {
            _client = new RestClient(_url);
        }
        public void CallPatientApi(DTO.Patient patient, string patientline)
        {
            string response = null;
            IRestResponse postResponse = null;

            string session = ConfigurationManager.AppSettings["ApiSessionPortal"].ToString();

            var request = new RestRequest("/api/patient", Method.POST);
            request.AddHeader("AUTHTOKEN", session);
            request.AddJsonBody(patient);

            postResponse = _client.Post(request);

            if (postResponse.StatusCode != HttpStatusCode.OK)
            {
                response = postResponse.StatusCode + ";" + postResponse.Content + ";" + postResponse.ErrorMessage;
            }
            
            if (response == null)
            {
                Logger.Write("Response: patient created " + patient.FirstName + " " + patient.LastName);
            }
            else
            {
                Logger.Write("Error: " + patient.FirstName + " " + patient.LastName + response);
            }

            LogResponse(response, patient, patientline);

            if (postResponse.StatusCode == HttpStatusCode.Unauthorized)
            {
                Logger.Write("Error: Unauthorized... ");
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }

        }

        public void CallPatientStatusApi(DTO.PatientStatus status)
        {
            string response = null;
            IRestResponse postResponse = null;

            string session = ConfigurationManager.AppSettings["ApiSessionPortal"].ToString();

            var request = new RestRequest("/api/patient/" + status.PatientId + "/status", Method.POST);
            request.AddHeader("AUTHTOKEN", session);
            request.AddJsonBody(status);

            postResponse = _client.Post(request);

            if (postResponse.StatusCode != HttpStatusCode.OK)
            {
                response = postResponse.StatusCode + ";" + postResponse.Content + ";" + postResponse.ErrorMessage;
            }

            if (response == null)
            {
                Logger.Write(status.PatientId + " patient status updated to " + status.Statusid);
            }
            else
            {
                Logger.Write("Error: " + status.PatientId  + response);
            }

            if (postResponse.StatusCode == HttpStatusCode.Unauthorized)
            {
                Logger.Write("Error: Unauthorized... ");
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
        }

        public void CallProviderApi(DTO.ProviderModel provider,string rowData)
        {
            string response = null;
            IRestResponse postResponse = null;

            string session = ConfigurationManager.AppSettings["ApiSessionPortal"].ToString();

            var request = new RestRequest("/api/provider", Method.POST);
            request.AddHeader("AUTHTOKEN", session);
            request.AddJsonBody(provider);

            postResponse = _client.Post(request);

            if (postResponse.StatusCode != HttpStatusCode.OK)
            {
                response = postResponse.StatusCode + ";" + postResponse.Content + ";" + postResponse.ErrorMessage;
            }

            if (response == null)
            {
                Logger.Write(provider.LastName  + " uploaded  "  );
            }
            else
            {
                Logger.Write("Error: " + provider.LastName  + " " + provider.FirstName + response);
                Logger.ErrorLog(rowData);
            }

            if (postResponse.StatusCode == HttpStatusCode.Unauthorized)
            {
                Logger.Write("Error: Unauthorized... ");
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
        }

        public void CallPatientServicelevelApi(DTO.PatientServiceLevel slModel)
        {
            string response = null;
            IRestResponse postResponse = null;

            string session = ConfigurationManager.AppSettings["ApiSessionPortal"].ToString();

            var request = new RestRequest("/api/patient/" + slModel.PatientId + "/serviceLevel", Method.PUT);
            request.AddHeader("AUTHTOKEN", session);
            request.AddJsonBody(slModel);

            postResponse = _client.Put(request);

            if (postResponse.StatusCode != HttpStatusCode.OK)
            {
                response = postResponse.StatusCode + ";" + postResponse.Content + ";" + postResponse.ErrorMessage;
            }

            if (response == null)
            {
                Logger.Write(slModel.PatientId + " service level assigned. " + slModel.ServiceLevelId);
            }
            else
            {
                Logger.Write("Error: " + slModel.PatientId + response);
            }

            if (postResponse.StatusCode == HttpStatusCode.Unauthorized)
            {
                Logger.Write("Error: Unauthorized... ");
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
        }

        public void UnassignServiceLevel(int patientid)
        {
          
            string response = null;
            IRestResponse postResponse = null;

            string session = ConfigurationManager.AppSettings["ApiSessionPortal"].ToString();

            var request = new RestRequest("/api/patient/" + patientid + "/serviceLevel/unassign", Method.PUT);
            request.AddHeader("AUTHTOKEN", session);
            

            postResponse = _client.Put(request);

            if (postResponse.StatusCode != HttpStatusCode.OK)
            {
                response = postResponse.StatusCode + ";" + postResponse.Content + ";" + postResponse.ErrorMessage;
            }

            if (response == null)
            {
                Logger.Write(patientid + " service level unassigned");
            }
            else
            {
                Logger.Write("Error: " + patientid + response);
            }

            if (postResponse.StatusCode == HttpStatusCode.Unauthorized)
            {
                Logger.Write("Error: Unauthorized... ");
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
        }

        public void CallCaregiverApi(DTO.Caregiver caregiver, string rowData)
        {
            string response = null;
            IRestResponse postResponse = null;

            string session = ConfigurationManager.AppSettings["ApiSessionPortal"].ToString();

            var request = new RestRequest("/api/caregiver", Method.POST);
            request.AddHeader("AUTHTOKEN", session);
            request.AddJsonBody(caregiver);

            postResponse = _client.Post(request);

            if (postResponse.StatusCode != HttpStatusCode.OK)
            {
                response = postResponse.StatusCode + ";" + postResponse.Content + ";" + postResponse.ErrorMessage;
            }

            if (response == null)
            {
                Logger.Write(caregiver.Last_Name + " uploaded  ");
            }
            else
            {
                Logger.Write("Error: " + caregiver.Last_Name + " " + caregiver.First_Name + response);
                Logger.ErrorLog(rowData);
            }

            if (postResponse.StatusCode == HttpStatusCode.Unauthorized)
            {
                Logger.Write("Error: Unauthorized... ");
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
        }

        public bool CheckLogTableExists()
        {
            bool exists;
            SqlConnection connection = null;

            try
            {
                using (connection = new SqlConnection(_connectionString))
                {
                    var cmd = new SqlCommand(
                                "select case when exists((select * from information_schema.tables where table_name = 'NewPatientResponses')) then 1 else 0 end", connection);
                    cmd.Connection.Open();
                    exists = (int)cmd.ExecuteScalar() == 1;

                }
            }
            catch(Exception ex)
            {
                Logger.Write("Error opening sql connection: " + ex.Message + ";" + ex.StackTrace);
                exists = false;
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }

            return exists;
        }

        public int UpdateMRN(DTO.MRN mrnObj)
        {            
            SqlConnection connection = null;

            int rowsAffected = 0;
            try
            {
                using (connection = new SqlConnection(_connectionString))
                {
                    
                    var cmd = new SqlCommand(
                                "Update tblPatient set mrnident = @newmrn where id =@patientid", connection);
                    cmd.Parameters.Add("@newmrn", SqlDbType.VarChar);
                    cmd.Parameters["@newmrn"].Value = mrnObj.NewMRN;
                    cmd.Parameters.Add("@patientid", SqlDbType.Int);
                    cmd.Parameters["@patientid"].Value = mrnObj.PatientId;

                    cmd.Connection.Open();
                    rowsAffected = cmd.ExecuteNonQuery();

                }
            }
            catch (Exception ex)
            {
                Logger.Write("Error opening sql connection: " + ex.Message + ";" + ex.StackTrace);
                 
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }

            return rowsAffected;
        }

        public void CallPatientProgramApi(DTO.PatientProgram program, string rowData)
        {
            string response = null;
            IRestResponse postResponse;

            string session = ConfigurationManager.AppSettings["ApiSessionPortal"].ToString();

            var request = new RestRequest("/api/cgp/patient/" + program.Id + "/demographics/program", Method.PUT);
            request.AddHeader("AUTHTOKEN", session);
            request.AddJsonBody(program);

            postResponse = _client.Put(request);

            if (postResponse.StatusCode != HttpStatusCode.OK)
            {
                response = postResponse.StatusCode + ";" + postResponse.Content + ";" + postResponse.ErrorMessage;
            }

            if (response == null)
            {
                Logger.Write(program.Id + " updated  ");
            }
            else
            {
                Logger.Write("Error: " + program.Id   + response);
                Logger.ErrorLog(rowData);
            }

            if (postResponse.StatusCode == HttpStatusCode.Unauthorized)
            {
                Logger.Write("Error: Unauthorized... ");
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
        }
        public void CallPatientHospitalApi(DTO.PatientHospital program, string rowData)
        {
            string response = null;
            IRestResponse postResponse;

            string session = ConfigurationManager.AppSettings["ApiSessionPortal"].ToString();

            var request = new RestRequest("/api/cgp/patient/" + program.Id + "/demographics/hospital", Method.PUT);
            request.AddHeader("AUTHTOKEN", session);
            request.AddJsonBody(program);

            postResponse = _client.Put(request);

            if (postResponse.StatusCode != HttpStatusCode.OK)
            {
                response = postResponse.StatusCode + ";" + postResponse.Content + ";" + postResponse.ErrorMessage;
            }

            if (response == null)
            {
                Logger.Write(program.Id + " updated  ");
            }
            else
            {
                Logger.Write("Error: " + program.Id + response);
                Logger.ErrorLog(rowData);
            }

            if (postResponse.StatusCode == HttpStatusCode.Unauthorized)
            {
                Logger.Write("Error: Unauthorized... ");
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
        }

        public void CallPatientSpecialHandlingApi(DTO.PatientSpecialHandling shandling, string rowData)
        {
            string response = null;
            IRestResponse postResponse;

            string session = ConfigurationManager.AppSettings["ApiSessionPortal"].ToString();

            var request = new RestRequest("/api/patient/" + shandling.PatientId + "/specialHandling", Method.POST);
            request.AddHeader("AUTHTOKEN", session);
            request.AddJsonBody(shandling);

            postResponse = _client.Post(request);

            if (postResponse.StatusCode != HttpStatusCode.OK)
            {
                response = postResponse.StatusCode + ";" + postResponse.Content + ";" + postResponse.ErrorMessage;
            }

            if (response == null)
            {
                Logger.Write(shandling.PatientId + " updated  ");
            }
            else
            {
                Logger.Write("Error: " + shandling.PatientId + response);
                Logger.ErrorLog(rowData);
            }

            if (postResponse.StatusCode == HttpStatusCode.Unauthorized)
            {
                Logger.Write("Error: Unauthorized... ");
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
        }

        public void LogResponse(string response, DTO.Patient patient, string patientline)
        {
            SqlConnection connection = null;
            try
            {
                using (connection = new SqlConnection(_connectionString))
                {
                    String querystring = "insert into [dbo].[NewPatientResponses] (Patient,LastName,Error,CreatedDate) Values (@patient,@lastname,@error, @created) ";

                    SqlCommand command = new SqlCommand(querystring, connection);
                    command.Parameters.Add("@patient", SqlDbType.VarChar);
                    command.Parameters.Add("@lastname", SqlDbType.VarChar, 50);
                    command.Parameters.Add("@error", SqlDbType.VarChar);
                    command.Parameters.Add("@created", SqlDbType.DateTime2);

                    command.Parameters["@patient"].Value = patientline;

                    command.Parameters["@lastname"].Value = (patient != null) ? patient.LastName : "";

                    if (string.IsNullOrEmpty(response))
                    {
                        command.Parameters["@error"].Value = DBNull.Value;
                    }
                    else
                    {
                        command.Parameters["@error"].Value = response;
                    }

                    command.Parameters["@created"].Value = DateTime.Now;
                    command.Connection.Open();
                    command.ExecuteNonQuery();

                }

                ThreadSleep();
            }
            catch (Exception ex)
            {
                Logger.Write(ex.Message);
                Logger.Detail(ex.StackTrace);
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
        }

        private void ThreadSleep()
        {
            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(Convert.ToInt32(ConfigurationManager.AppSettings["sleep"])));
        }
    }

}

