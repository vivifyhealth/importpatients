﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq; 
using System.Threading.Tasks;
using System.Web.Http;

namespace ImportPatients
{
    class UpdatePatientStatus
    {
        public UpdatePatientStatus()
        {
        } 
 

        public void PostStatus(string file)
        {
          
            DataTable data = Common.GetData(file, true);
            List<DataTable> tables = UploadPatients.SplitTable(data, Convert.ToInt32(ConfigurationManager.AppSettings["batchSize"]));
            try
            {

                foreach (DataTable dt in tables)
                {
                    Parallel.ForEach(dt.AsEnumerable(), dr =>
                    {
                        var status = new DTO.PatientStatus();
                        string rowData = "";
                        int id = DataTypes.GetIntData(dr["autoID"]);

                        try
                        {
                            GetPatientStatusData(status, dr);

                            if (status.PatientId > 0)
                            {
                                rowData = "RowNumber:" + id + "; PatientId: " + status.PatientId + "," + status.Statusid + "," + status.StatusChangeReasonId;
                                Logger.Write(rowData);
                                VivifyRestClient client = new VivifyRestClient();
                                client.CallPatientStatusApi(status);
                            }

                        }
                        catch (HttpResponseException unauthex)
                        {
                            Logger.Write("Error " + unauthex.Message);
                            tables.Clear();
                            dt.Clear();
                        }
                        catch (Exception ex)
                        {
                            Logger.Write("Error " + ex.Message);
                        }
                    });
                }
                Logger.Write("Upload Complete.");
            }
            catch (Exception ex)
            {
                Logger.Write("Error: " + ex.Message);
                Logger.Detail(ex.StackTrace);
            }
        }


        private void GetPatientStatusData(DTO.PatientStatus status, DataRow dr)
        {
            status.IsActivePatient = false;
            status.MostRecentRecord = false;
            status.OriginalMonitoringStatusId = 0;
            status.ReasonText = "";
            status.DeletedBy_Id = null;
            status.DeletedDateTime_UTC = null;
            status.CreatedDtUTC = DateTime.UtcNow;

            foreach (DataColumn item in dr.Table.Columns)
            {
                switch (item.ColumnName.ToLower())
                {
                    case "statusid":
                    case "status":
                        {
                            status.Statusid = (DTO.MonitoringStatusEnum)DataTypes.GetIntData(dr[item]);                             
                            if (status.Statusid != DTO.MonitoringStatusEnum.Deleted)//not deleted
                            {
                                status.IsFinalState = false;
                            }
                            else
                            {
                                status.IsFinalState = true;
                            }
                        }
                        break;
                    case "patientid":
                        {
                            status.PatientId = DataTypes.GetIntData(dr[item]);
                        }

                        break;
                    case "reason":
                    case "reasonid":
                        {
                            status.StatusChangeReasonId = DataTypes.GetIntData(dr[item]);
                        }

                        break;
                    case "reasontext":
                        {
                            status.ReasonText = DataTypes.GetStringData(dr[item]);
                        }

                        break;

                    default:
                        break;
                };
            }

        }

     
    }
}
