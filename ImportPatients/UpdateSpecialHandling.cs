﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;

namespace ImportPatients
{
    class UpdateSpecialHandling
    {
        public UpdateSpecialHandling()
        {
        }
        public void PostSpecialHandling(string file, bool create)
        {

            DataTable data = Common.GetData(file, true);
            List<DataTable> tables = UploadPatients.SplitTable(data, Convert.ToInt32(ConfigurationManager.AppSettings["batchSize"]));
            try
            {

                foreach (DataTable dt in tables)
                {
                    Parallel.ForEach(dt.AsEnumerable(), dr =>
                    {
                        var specialHandling = new DTO.PatientSpecialHandling();
                        string rowData = "";
                        int id = DataTypes.GetIntData(dr["autoID"]);

                        try
                        {
                            GetSpecialHandlingData(specialHandling, dr);

                            if (specialHandling.SpecialHandlingId > 0)
                            {
                                rowData = "RowNumber:" + id + " ," + specialHandling.PatientId + "," + specialHandling.SpecialHandlingId;
                                Logger.Write(rowData);
                                if (create)
                                {
                                    VivifyRestClient client = new VivifyRestClient();
                                    client.CallPatientSpecialHandlingApi(specialHandling, rowData);
                                }
                            }

                        }
                        catch (HttpResponseException unauthex)
                        {
                            Logger.Write("Error " + unauthex.Message);
                            tables.Clear();
                            dt.Clear();

                        }
                        catch (Exception ex)
                        {
                            Logger.Write("Error " + ex.Message);
                            Logger.Detail(ex.StackTrace);
                        }
                    });

                    Logger.Write("Do you wish to continue? Press X to exit.");
                    while (Console.ReadKey(true).Key == ConsoleKey.X)
                    {
                        Common.WriteToLog();
                        Environment.Exit(0);
                    }
                }
                Logger.Write("Upload Complete.");
            }
            catch (Exception ex)
            {
                Logger.Write("Error: " + ex.Message);
                Logger.Detail(ex.StackTrace);
            }
        }


        private void GetSpecialHandlingData(DTO.PatientSpecialHandling pg, DataRow dr)
        {

            foreach (DataColumn item in dr.Table.Columns)
            {
                switch (item.ColumnName.ToLower())
                {
                    case "patientid":
                        {
                            pg.PatientId = (int)DataTypes.GetIntDataWithNull(dr[item]);

                        }
                        break;
                    case "specialhandlingid":
                        {
                            pg.SpecialHandlingId = (int)DataTypes.GetIntDataWithNull(dr[item]);

                        }
                        break;


                    default:
                        break;
                };
            }

        }
    }
}
