﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ImportPatients
{
    internal static class DataTypes
    {
        public static int GetIntData(object data)
        {
            int result = 0;

            if (data != null)
            {
                string value = data.ToString();
                if (!String.IsNullOrEmpty(value))
                {
                    value = Regex.Replace(value, "Null", "0", RegexOptions.IgnoreCase);
                    result = Convert.ToInt32(value);
                }
            }

            return result;
        }

        public static int? GetIntDataWithNull(object data)
        {
            int? result = 0;

            if (data != null)
            {
                string value = data.ToString();
                if (!String.IsNullOrEmpty(value))
                {
                    value = Regex.Replace(value, "Null", "0", RegexOptions.IgnoreCase);
                    result = Convert.ToInt32(value);
                    result = result > 0 ? result : null;
                }
                else
                {
                    return null;
                }
            }

            return result;
        }

        public static string GetStringData(object data)
        {
            string result = "";

            if (data != null)
            {
                string value = data.ToString();
                if (value.Trim() != "")
                {
                    result = Regex.Replace(value, "Null", "", RegexOptions.IgnoreCase);
                }
            }

            return result;
        }

        public static bool GetBooleanData(object data)
        {
            bool result = false;
            if (data != null)
            {
                string value = data.ToString().ToLower().Trim();
                if(value == "true")
                {
                    result = true;
                }
              
            }
            return result;
        }
    }
}
