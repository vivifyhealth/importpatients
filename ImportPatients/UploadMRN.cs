﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Web.Http;

namespace ImportPatients
{
    class UploadMRN
    {
        public void PostMRNUpdate(string file)
        {
            
            DataTable data = Common.GetData(file, true);
            List<DataTable> tables = UploadPatients.SplitTable(data, Convert.ToInt32(ConfigurationManager.AppSettings["batchSize"]));
            try
            {

                foreach (DataTable dt in tables)
                {
                    Parallel.ForEach(dt.AsEnumerable(), dr =>
                    {
                        var mrnObj = new DTO.MRN();
                        string rowData = "";
                        int id = DataTypes.GetIntData(dr["autoID"]);

                        try
                        {
                            GetMRNData(mrnObj, dr);

                            if (mrnObj.PatientId > 0)
                            {
                                rowData = "RowNumber:" + id + "; PatientId: " + mrnObj.PatientId;
                                Logger.Write(rowData);
                                VivifyRestClient client = new VivifyRestClient();
                                int updated = client.UpdateMRN(mrnObj);
                                
                                Logger.Write("PatientId: " + mrnObj.PatientId + " mrn updated  " + updated);

                                //LogLocalHost()
                                 
                            }

                        }
                        catch (HttpResponseException unauthex)
                        {
                            Logger.Write("Error " + unauthex.Message);
                            tables.Clear();
                            dt.Clear();
                        }
                        catch (Exception ex)
                        {
                            Logger.Write("Error " + ex.Message);
                        } 
                    });

                    Logger.Write("Do you wish to continue? Press X to exit.");
                    while (Console.ReadKey(true).Key == ConsoleKey.X)
                    {
                        Common.WriteToLog();
                        Environment.Exit(0);
                    }
                }
                Logger.Write("Upload Complete.");
            }
            catch (Exception ex)
            {
                Logger.Write("Error: " + ex.Message);
                Logger.Detail(ex.StackTrace);
            }
        }


        private void GetMRNData(DTO.MRN mrn, DataRow dr)
        { 
            foreach (DataColumn item in dr.Table.Columns)
            {
                switch (item.ColumnName.ToLower())
                {
                     
                    case "patientid":
                        {
                            mrn.PatientId = DataTypes.GetIntData(dr[item]);
                        }

                        break;
                    case "oldmrn":                    
                        {
                            mrn.OldMRN = DataTypes.GetStringData(dr[item]);
                        }

                        break;
                    case "newmrn":
                        {
                            mrn.NewMRN = DataTypes.GetStringData(dr[item]);
                        }

                        break;

                    default:
                        break;
                };
            }

        }
    }
}
