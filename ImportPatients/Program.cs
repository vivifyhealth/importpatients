﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportPatients
{
    class Program
    {
        static void Main(string[] args)
        {
            //args = new string[] { @"C:\Users\pdasired\Repos\importpatients\ImportPatients\bin\Debug\programchanges.xlsx" }; //this is for debugging do not push this change
            Common _common = new Common();
            string file = args[0];
            string configParams;

            Dictionary<string, string> options = new Dictionary<string, string>()
            {
                {"c","Check phone and dob are present and validate" }
                ,{"n","Read the patient upload data to verify the code is pulling up all fields" }
                ,{"p","upload patients to portal" }
                ,{"e","verify if patient already exists" }
                ,{"status" ,"Monitoring status Change"}
                ,{"mrn", "Update MRN" }
                ,{"pu" , "Provider upload"}
                ,{"pr" , "Provider read"}
                ,{"cu" , "CTM upload"}
                ,{"cr" , "CTM read"}
                ,{"sl", "Service Level assign +GO" }
                ,{"nosl", "Unassign Service Level +GO" }
                ,{"pgu" , "program update... make sure status is not screenfail"}
                ,{"pgr" , "program read"}
                ,{"hr" , "patient hospital read"}
                ,{"hu" , "patient hospital update"}
                ,{"shr" , "special handling read"}
                ,{"shu" , "special handling update"}
            };

            //Print the options to console
            Console.Write(Environment.NewLine + "Available option..." + Environment.NewLine);
            Console.Write("/******************************/" + Environment.NewLine);
            foreach (KeyValuePair<string, string> kvp in options)
            {
                Console.WriteLine("Option : {0} - {1}", kvp.Key, kvp.Value);
            }
            Console.Write("/******************************/" + Environment.NewLine);

            Console.Write("Enter the option...: ");
            string isUpload = Console.ReadLine();

            if (!options.ContainsKey(isUpload.ToLower()))
            {
                Console.Write("Not a valid option");
                Common.WriteToLog();
                Environment.Exit(0);
            }
            else
            {

                string token = ConfigurationManager.AppSettings["ApiSessionPortal"].ToString();
                token = _common.GetSiteName(token);


                configParams = "\n Please verify the configuration."
                    + "\n ApiSessionPortal:" + ConfigurationManager.AppSettings["ApiSessionPortal"]
                    + "\n PortalUrl:       " + ConfigurationManager.AppSettings["portalUrl"]
                    + "\n connectionstring:" + ConfigurationManager.AppSettings["connectionstring"]
                    + "\n sleep:           " + ConfigurationManager.AppSettings["sleep"]
                    + "\n batchSize:       " + ConfigurationManager.AppSettings["batchSize"]
                    + "\n file:            " + file
                    + "\n Option:          " + isUpload.ToUpper() + " - " + options[isUpload] + token;

                Logger.Write(configParams);
                Logger.Write("\n Verify if you have access to all hospitals, sites and security roles. Press Enter to continue and X to exit. ");
                while (Console.ReadKey(true).Key == ConsoleKey.X)
                {
                    Environment.Exit(0);
                }

                var watch = new System.Diagnostics.Stopwatch();
                watch.Start();

                CreateInstance _instance = new CreateInstance();
                bool found = _instance.CreateUploadInstance(isUpload, file);

                if (!found)
                {
                    Console.Write("Option not found");
                }


                Common.WriteToLog();
                watch.Stop();
                Logger.Write("Execution Time: " + "Hours:" + watch.Elapsed.Hours + ",mins : " + watch.Elapsed.Minutes + ",sec : " + watch.Elapsed.Seconds);
            }

            while (Console.ReadKey(true).Key == ConsoleKey.X)
            {
                Common.WriteToLog();
                Environment.Exit(0);
            }
        }

}

   

    public class CreateInstance
    {
        public CreateInstance()
        {

        }

        public bool CreateUploadInstance(string isUpload, string file)
        {
            bool result = false;
            switch (isUpload)
            {
                case "p":
                case "n":
                case "c":
                case "e":
                    {
                        UploadPatients patients = new UploadPatients();

                        if (isUpload == "p")//Post the data to server
                        {
                            Logger.Write("\n Upload started...");
                            patients.LoadExcelParallel(file, true);
                        }
                        else if (isUpload == "n")//Read the data to check for issues
                        {
                            Logger.Write("\n Reading started...");
                            patients.LoadExcelParallel(file, false);
                        }
                        else if (isUpload == "c")//Verify if DOB or phone data has issues reading from excel due to bad data formats.
                        {
                            Logger.Write("\n Verifying...");
                            patients.CheckDOBAndPhone(file);
                        }
                        else if (isUpload == "e")//Verify if DOB or phone data has issues reading from excel due to bad data formats.
                        {
                            Logger.Write("\n Verify patients exists...");
                            patients.CheckPatientsExist(file);
                        }
                        result = true;                       
                    }
                    break;
                case "status"://Update patient status ex: change status to deleted.
                    {
                        Logger.Write("\n Update started...");
                        UpdatePatientStatus status = new UpdatePatientStatus();
                        status.PostStatus(file);
                        result = true;                        
                    }
                    break;
                case "pu":
                    {
                        Logger.Write("\n Provider upload started...");
                        UploadProviders providers = new UploadProviders();
                        providers.PostProvider(file, true);
                        result = true;                       
                    }
                    break;
                case "pr":
                    {
                        Logger.Write("\n Provider data read started...");
                        UploadProviders providers = new UploadProviders();
                        providers.PostProvider(file, false);
                        result = true;                       
                    }
                    break;
                case "mrn":
                    {
                        Logger.Write("\n MRN Update started...");
                        UploadMRN _mrnUpload = new UploadMRN();
                        _mrnUpload.PostMRNUpdate(file);
                        result = true;                        
                    }
                    break;
                case "cu":
                    {
                        Logger.Write("\n CTM Upload started...");
                        UploadCTM _ctmUpload = new UploadCTM();
                        _ctmUpload.PostCTMs(file,true);
                        result = true;                        
                    }
                    break;
                case "cr":
                    {
                        Logger.Write("\n CTM data read started...");
                        UploadCTM _ctmUpload = new UploadCTM();
                        _ctmUpload.PostCTMs(file, false);
                        result = true;
                    }
                    break;
                case "sl":
                    {
                        Logger.Write("\n Service level upload...");
                        UploadServiceLevel _slUpload = new UploadServiceLevel();
                        _slUpload.PostServiceLevel(file, true);
                        result = true;                         
                    }
                    break;
                case "nosl":
                    {
                        Logger.Write("\n unassign Service level ...");
                        UploadServiceLevel _slUpload = new UploadServiceLevel();
                        _slUpload.UnassignServiceLevel(file);
                        result = true;
                    }
                    break;
                case "pgu":
                    {
                        Logger.Write("\n program update ...");
                        UpdateProgram _pg = new UpdateProgram();
                        _pg.PostNewProgram(file,true);
                        result = true;
                    }
                    break;
                case "pgr":
                    {
                        Logger.Write("\n program read ...");
                        UpdateProgram _pg = new UpdateProgram();
                        _pg.PostNewProgram(file,false);
                        result = true;
                    }
                    break;
                case "hr":
                    {
                        Logger.Write("\n hospital read ...");
                        UpdateHospital _hs = new UpdateHospital();
                        _hs.PostNewHospital(file, false);
                        result = true;
                    }
                    break;
                case "hu":
                    {
                        Logger.Write("\n hospital update ...");
                        UpdateHospital _hs = new UpdateHospital();
                        _hs.PostNewHospital(file, true);
                        result = true;
                    }
                    break;
                case "shr":
                    {
                        Logger.Write("\n special handling read ...");
                        UpdateSpecialHandling _sh = new UpdateSpecialHandling();
                        _sh.PostSpecialHandling(file, false);
                        result = true;
                    }
                    break;
                case "shu":
                    {
                        Logger.Write("\n special handling update ...");
                        UpdateSpecialHandling _sh = new UpdateSpecialHandling();
                        _sh.PostSpecialHandling(file, true);
                        result = true;
                    }
                    break;
                default:
                    break;
            }
            return result;
        }


     
    }
}