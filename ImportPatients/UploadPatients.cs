﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using System.Text.RegularExpressions;
using DataTable = System.Data.DataTable;
using System.Web.Http;
using System.Data.SqlClient;
using ImportPatients.DTO;

namespace ImportPatients
{
    class UploadPatients
    {
        private readonly RestClient _client;
        private readonly string _url = ConfigurationManager.AppSettings["portalUrl"];

        public UploadPatients()
        {
            _client = new RestClient(_url);
        }

        public void LoadExcelParallel(string file, bool createPatient)
        {
            VivifyRestClient vivifyClient = new VivifyRestClient();
            if (vivifyClient.CheckLogTableExists())
            {
                DataTable data = Common.GetData(file, true);
                List<DataTable> tables = SplitTable(data, Convert.ToInt32(ConfigurationManager.AppSettings["batchSize"]));
                try
                {
                    foreach (DataTable dt in tables)
                    {

                        Parallel.ForEach(dt.AsEnumerable(), dr =>
                        {
                            var patient = new DTO.Patient();
                            string rowData = "";
                            int id = DataTypes.GetIntData(dr["autoID"]);

                            try
                            {
                                GetPatientData(patient, dr);

                                if (!String.IsNullOrEmpty(patient.LastName))
                                {
                                    rowData = "RowNumber:" + id + "," +
                                       patient.FirstName + "," +
                                       patient.LastName + "," +
                                       patient.Middle_Name + "," +
                                       patient.Gender + "," +
                                       patient.DOB + "," +
                                       patient.Address1 + "," +
                                       patient.Address2 + "," +
                                       patient.State + "," +
                                       patient.City + "," +
                                       patient.ZipCode + "," +
                                       patient.Email + "," +
                                       patient.Phone1 + "," +
                                       patient.Phone2 + "," +
                                       patient.ServiceLevelId + "- +Go," +
                                       patient.ServiceLevelTypeId + "," +
                                       patient.TimeZoneId + "," +
                                       patient.HospitalId + "," +
                                       patient.PopulationId + "," +
                                       patient.PopulationGroupId + "," +
                                       patient.CustomField + "," +
                                       patient.PayerId + "," +
                                       patient.ReferralId + "," +
                                       patient.ProviderGroupId + "," +
                                       patient.LanguageId + "," +
                                       patient.RaceId + "," +                                     
                                       patient.MRNIdent;

                                    if (patient.Provider != null)
                                    {
                                        rowData += "," + patient.Provider.ProviderMasterId;
                                    }

                                    if (patient.PatientCaregiverList != null)
                                    {
                                        foreach (var c in patient.PatientCaregiverList)
                                        {
                                            rowData += "," + c.CaregiverId;
                                        }
                                    }

                                    Logger.Write(rowData);
                                    if (createPatient)
                                    {
                                        vivifyClient.CallPatientApi(patient, rowData);
                                    }
                                }

                            }
                            catch (HttpResponseException unauthex)
                            {
                                vivifyClient.LogResponse(unauthex.Message, patient, rowData);
                                tables.Clear();
                                dt.Clear();
                            }
                            catch (Exception ex)
                            {
                                Logger.Write("Error " + ex.Message);
                                vivifyClient.LogResponse(ex.Message, patient, rowData);
                            }
                        });

                        Logger.Write("Do you wish to continue? Press X to exit.");
                        while (Console.ReadKey(true).Key == ConsoleKey.X)
                        {
                            Common.WriteToLog();
                            Environment.Exit(0);
                        }
                    }
                    Logger.Write("Upload Complete.");
                }
                catch (Exception ex)
                {
                    Logger.Write("Error: " + ex.Message);
                    Logger.Detail(ex.StackTrace);
                }

                Logger.Write(" Press X to exit. ");
            }
            else
            {
                Logger.Write("NewPatientResponses table missing.");
            }
        }

        private void GetPatientData(DTO.Patient patient, DataRow dr)
        {
            foreach (DataColumn item in dr.Table.Columns)
            {
                try
                {
                    switch (item.ColumnName.ToLower())
                    {
                        case "firstname":
                        case "first name":
                            {
                                patient.FirstName = DataTypes.GetStringData(dr[item]);
                            }
                            break;
                        case "lastname":
                        case "last name":
                            {
                                patient.LastName = DataTypes.GetStringData(dr[item]);
                            }
                            break;
                        case "middlename":
                        case "middle name":
                            {
                                patient.Middle_Name = DataTypes.GetStringData(dr[item]);
                            }
                            break;                       
                        case "gender":
                        case "sex":
                            {
                                string gender = DataTypes.GetStringData(dr[item]);
                                patient.Gender = Common.GetGender(gender);
                            }
                            break;
                        case "dob":
                            {
                                string dob = dr[item].ToString().Trim();
                                if (!String.IsNullOrEmpty(dob))
                                {
                                    //Logger.Write("dob:" + dob);
                                    patient.DOB = DateTime.Parse(dob);
                                }
                            }
                            break;
                        case "address1":
                            {
                                patient.Address1 = DataTypes.GetStringData(dr[item]);
                            }
                            break;
                        case "address2":
                            {
                                patient.Address2 = DataTypes.GetStringData(dr[item]);
                            }
                            break;
                        case "state":
                            {
                                patient.State = DataTypes.GetStringData(dr[item]);
                            }
                            break;
                        case "city":
                            {
                                patient.City = DataTypes.GetStringData(dr[item]);
                            }
                            break;
                        case "zip":
                        case "zipcode":
                            {
                                patient.ZipCode = DataTypes.GetStringData(dr[item]);
                            }
                            break;
                        case "email":
                            {
                                patient.Email = DataTypes.GetStringData(dr[item]);
                            }
                            break;
                        case "phone1":
                            {
                                patient.Phone1 = String.IsNullOrEmpty(dr[item].ToString())
                                    ? ""
                                    : Regex.Replace(dr[item].ToString(), "[^0-9]", "");
                                if (patient.Phone1.Length == 1)
                                {
                                    patient.Phone1 = "";
                                }
                                else if (patient.Phone1.Length == 10)
                                {
                                    patient.Phone1 = "1" + patient.Phone1;
                                }
                            }
                            break;
                        case "phone2":
                            {
                                patient.Phone2 = String.IsNullOrEmpty(dr[item].ToString())
                                    ? ""
                                    : Regex.Replace(dr[item].ToString(), "[^0-9]", "");
                                if (patient.Phone2.Length == 1)
                                {
                                    patient.Phone2 = "";
                                }
                                else if (patient.Phone2.Length == 10)
                                {
                                    patient.Phone2 = "1" + patient.Phone2;
                                }
                            }
                            break;
                        case "servicelevelid":
                            {
                                patient.ServiceLevelId =
                                    DataTypes.GetIntData(dr[item]); // defines GO monitor/engage/guide ServiceLevel table
                            }
                            break;
                        case "serviceleveltypeid":
                            {
                                patient.ServiceLevelTypeId =
                                    DataTypes.GetIntData(dr[item]); // defines +GO ServiceLevelType_Enum table
                            }
                            break;
                        case "timezoneid":
                            {
                                patient.TimeZoneId = DataTypes.GetIntData(dr[item]);
                            }
                            break;
                        case "hospitalid":
                            {
                                patient.HospitalId = DataTypes.GetIntData(dr[item]);
                            }
                            break;
                        case "populationid":
                        case "programid":
                            {
                                patient.PopulationId = DataTypes.GetIntData(dr[item]);
                            }
                            break;
                        case "mrn":
                        case "mrnident":
                            {
                                patient.MRNIdent = DataTypes.GetStringData(dr[item]);
                            }
                            break;
                        case "programgroupid":
                        case "populationgroupid":
                            {
                                patient.PopulationGroupId = DataTypes.GetIntData(dr[item]);
                            }
                            break;
                        case "raceid":
                            {
                                patient.RaceId = DataTypes.GetIntData(dr[item]);
                            }
                            break;
                        case "payerid":
                            {
                                patient.PayerId = DataTypes.GetIntData(dr[item]);
                            }
                            break;
                        case "referralid":
                            {
                                patient.ReferralId = DataTypes.GetIntData(dr[item]);
                            }
                            break;
                        case "languageid":
                            {
                                patient.LanguageId = DataTypes.GetIntData(dr[item]);
                            }
                            break;
                        case "studyid":
                        case "customfield":
                            {
                                patient.CustomField = DataTypes.GetStringData(dr[item]);
                            }
                            break;
                        case "providergroupid":
                            {
                                patient.ProviderGroupId = DataTypes.GetIntData(dr[item]);
                            }
                            break;
                        case "providermasterid":
                        case "providerid":
                            {
                                //ProviderMasterId from provider table is needed to update provider on the patient
                                patient.Provider = new DTO.ProviderModel();
                                patient.Provider.ProviderMasterId = DataTypes.GetIntData(dr[item]);
                                patient.Provider.FreeText = false;
                            }
                            break;
                        case "patientcaregivers":
                            {
                                Array objcaregivers = dr[item].ToString().Split(',');
                                patient.PatientCaregiverList = new List<DTO.PatientCaregiverModel>();
                                foreach (string _obj in objcaregivers)
                                {
                                    DTO.PatientCaregiverModel modelCaregiver = new DTO.PatientCaregiverModel();
                                    modelCaregiver.CaregiverId = DataTypes.GetIntData(_obj);
                                    modelCaregiver.PatientId = 1;//this will be reassigned after patient is created.
                                    patient.PatientCaregiverList.Add(modelCaregiver);
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Write("Error parsing " + item.ColumnName.ToLower() + ": " + ex.Message);
                    Logger.Detail(ex.StackTrace);
                }
            }

            if (String.IsNullOrEmpty(patient.Phone1))
            {
                patient.ServiceLevelTypeId = 0;
            }
        }

        public static List<DataTable> SplitTable(DataTable originalTable, int batchSize)
        {
            List<DataTable> tables = new List<DataTable>();
            int i = 0;
            int j = 1;
            int limit = originalTable.Rows.Count;

            DataTable newDt = originalTable.Clone();
            newDt.TableName = "Table_" + j;
            newDt.Clear();

            foreach (DataRow row in originalTable.Rows)
            {
                DataRow newRow = newDt.NewRow();
                newRow.ItemArray = row.ItemArray;
                newDt.Rows.Add(newRow);

                i++;

                if (i == batchSize)
                {
                    tables.Add(newDt);
                    j++;
                    newDt = originalTable.Clone();
                    newDt.TableName = "Table_" + j;
                    newDt.Clear();
                    i = 0;
                }
            }

            if (newDt.Rows.Count > 0)
            {
                tables.Add(newDt);
                j++;
                newDt = originalTable.Clone();
                newDt.TableName = "Table_" + j;
                newDt.Clear();
            }

            return tables;
        }



        public System.Data.DataTable GetDataTable(string file)
        {
            System.Data.DataTable dt = new DataTable();
            using (var reader = new ExcelDataReader(file))
                dt.Load(reader);

            return dt;
        }


        public void CheckDOBAndPhone(string file)
        {
            System.Data.DataTable data = Common.GetData(file, true);
            List<DataTable> tables = SplitTable(data, Convert.ToInt32(ConfigurationManager.AppSettings["batchSize"]));

            foreach (DataTable dt in tables)
            {
                Parallel.ForEach(dt.AsEnumerable(), dr =>
                {
                    var patient = new DTO.Patient();
                    string rowData = "";
                    int id = DataTypes.GetIntData(dr["autoID"]);
                    try
                    {
                        GetPatientData(patient, dr);
                        if (!String.IsNullOrEmpty(patient.LastName))
                        {
                            if (!patient.DOB.HasValue)
                            {
                                rowData = "RowNumber:" + id + ",Lastname:" + patient.LastName + "DOB:" + patient.DOB;
                                Logger.Write(rowData);
                            }
                            if (String.IsNullOrEmpty(patient.Phone1))
                            {
                                rowData = "RowNumber:" + id + ",Lastname:" + patient.LastName + ", Phone1:" + patient.Phone1;
                                Logger.Write(rowData);
                            }
                            if (String.IsNullOrEmpty(patient.Phone2))
                            {
                                rowData = "RowNumber:" + id + ",Lastname:" + patient.LastName + ", Phone2:" + patient.Phone2;
                                Logger.Write(rowData);
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        Logger.Write("Error " + ex.Message + ex.StackTrace);
                    }

                });
            }

            Logger.Write("Check Complete. Press Enter or X to exit. ");


        }


        public void CheckPatientsExist(string file)
        {
            SqlConnection connection = null;
            DataSet ds = new DataSet();
            List<Patient> _listp = new List<Patient>();
            try
            {
                using (connection = new SqlConnection(ConfigurationManager.AppSettings["connectionString"]))
                {
                    String sql = "select * from tblpatient";
                    SqlCommand command = new SqlCommand(sql, connection);

                    command.Connection.Open();
                    SqlCommand cmd = new SqlCommand(sql, connection);
                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Patient p = new Patient();
                        p.FirstName = row["First_Name"].ToString();
                        p.LastName = row["Last_Name"].ToString();
                        p.Phone1 = row["Phone1"].ToString();

                        if (!DBNull.Value.Equals(row["DOB_UTC"]))
                        {
                            p.DOB = Convert.ToDateTime(row["DOB_UTC"]);
                        }

                        _listp.Add(p);
                    }

                }

                DataTable data = Common.GetData(file, true);
                foreach (DataRow dr in data.Rows)
                {
                    Patient pt = _listp
                        .Where(p => p.FirstName.Trim() == dr["FirstName"].ToString().Trim()
                                 && p.LastName.Trim() == dr["LastName"].ToString().Trim()
                                 && p.Phone1.EndsWith(dr["Phone1"].ToString())
                              )
                        .FirstOrDefault();


                    if (pt != null)
                    {
                        Logger.Write("Patient exists : " + pt.FirstName + ":" + pt.LastName + ":" + pt.Phone1);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(ex.Message);
                Logger.Detail(ex.StackTrace);
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
        }
    }
}
