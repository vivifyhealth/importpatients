﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;

namespace ImportPatients
{
    class UploadCTM
    {
        public void PostCTMs(string file, bool create)
        {

            DataTable data = Common.GetData(file, true);
            List<DataTable> tables = UploadPatients.SplitTable(data, Convert.ToInt32(ConfigurationManager.AppSettings["batchSize"]));
            try
            {

                foreach (DataTable dt in tables)
                {
                    Parallel.ForEach(dt.AsEnumerable(), dr =>
                    {
                        var caregiver = new DTO.Caregiver();
                        string rowData = "";
                        int id = DataTypes.GetIntData(dr["autoID"]);

                        try
                        {
                            GetCTMData(caregiver, dr);

                            if (caregiver.Last_Name.Length > 0)
                            {

                                rowData = "RowNumber:" + id + " ," + caregiver.First_Name + "," + caregiver.Last_Name + "," + caregiver.Middle_Name + "," + caregiver.User_Name + " ,"
                                            + caregiver.Gender + " ," + caregiver.Password + " ," + caregiver.CaregiverTitleId + " ," + caregiver.TimeZoneId 
                                            + " , Roles:" + dr["SecurityRole"].ToString() + " , Sites:" + dr["sites"].ToString() + " , Hospitals:" + dr["hospitalid"].ToString() + " ,"
                                            + caregiver.Email + " ," + caregiver.Phone1 + " , Auth:" + caregiver.AuthorizationModeId + " ,"
                                            + caregiver.ReceiveEmail + " ," + caregiver.ReceiveLogisticsMessages + " ," + caregiver.IsShownInTimeLog + " ,"
                                            + caregiver.ExpirePasswordNow + " ," + caregiver.CreateUniversityUser;

                                Logger.Write(rowData);
                                if (create)
                                {
                                    VivifyRestClient client = new VivifyRestClient();
                                    client.CallCaregiverApi(caregiver, rowData);
                                }

                            }
                        }
                        catch (HttpResponseException unauthex)
                        {
                            Logger.Write("ID: " + id + ". Error " + unauthex.Message);
                            tables.Clear();
                            dt.Clear();
                        }
                        catch (Exception ex)
                        {
                            Logger.Write("ID: " + id + ". Error " + ex.Message);
                        }
                    });

                    Logger.Write("Do you wish to continue? Press X to exit.");
                    while (Console.ReadKey(true).Key == ConsoleKey.X)
                    {
                        Common.WriteToLog();
                        Environment.Exit(0);
                    }
                }
                Logger.Write("Upload Complete.");
            }
            catch (Exception ex)
            {
                Logger.Write("Error: " + ex.Message);
                Logger.Detail(ex.StackTrace);
            }
        }


        private void GetCTMData(DTO.Caregiver obj, DataRow dr)
        {
            obj.IsThirdParty = false;
            foreach (DataColumn item in dr.Table.Columns)
            {
                switch (item.ColumnName.ToLower())
                {
                    case "username":
                        {
                            obj.User_Name = DataTypes.GetStringData(dr[item]);
                        }
                        break;
                    case "lastname":
                        {
                            obj.Last_Name = DataTypes.GetStringData(dr[item]);
                        }

                        break;
                    case "firstname":
                        {
                            obj.First_Name = DataTypes.GetStringData(dr[item]);
                        }
                        break;
                    case "middlename":
                        {
                            obj.Middle_Name = DataTypes.GetStringData(dr[item]);
                        }
                        break;
                    case "gender":
                        {
                            string gender = DataTypes.GetStringData(dr[item]);
                            obj.Gender = Common.GetGender(gender);
                        }
                        break;
                    case "authorizationmode":
                        {
                            obj.AuthorizationModeId = DataTypes.GetIntData(dr[item]);
                        }
                        break;
                    case "email":
                        {
                            obj.Email = DataTypes.GetStringData(dr[item]);
                        }
                        break;
                    case "timezoneid":
                        {
                            obj.TimeZoneId = DataTypes.GetIntData(dr[item]);
                        }
                        break;
                    case "phone":
                        {
                            obj.Phone1 = Common.GetPhone(dr[item]);
                        }
                        break;
                    case "password":
                        {
                            obj.Password = DataTypes.GetStringData(dr[item]);
                        }
                        break;
                    case "caregivertitleid":
                        {
                            obj.CaregiverTitleId = DataTypes.GetIntData(dr[item]);
                        }
                        break;
                    case "hospitalid":
                        {
                            Array objHospitals = dr[item].ToString().Split(',');
                            foreach (string _hosp in objHospitals)
                            {
                                DTO.CaregiverHospitalModel modelHospital = new DTO.CaregiverHospitalModel();
                                modelHospital.Id = DataTypes.GetIntData(_hosp);
                                obj.CaregiverHospitals.Add(modelHospital);
                            }
                        }
                        break;
                    case "securityrole":
                        {
                            Array securityroles = dr[item].ToString().Split(',');
                            foreach (string role in securityroles)
                            {
                                DTO.SecurityRoleModel modelRole = new DTO.SecurityRoleModel();
                                modelRole.SecurityRoleId = DataTypes.GetIntData(role);
                                obj.SecurityRoles.Add(modelRole);
                            }
                        }
                        break;
                    case "sites":
                        {
                            Array sites = dr[item].ToString().Split(',');
                            foreach (string site in sites)
                            {
                                DTO.CaregiverSiteModel modelSite = new DTO.CaregiverSiteModel();
                                modelSite.SiteId = DataTypes.GetIntData(site);
                                obj.CaregiverSites.Add(modelSite);
                            }
                        }
                        break;
                    case "receiveemail":
                        {
                            obj.ReceiveEmail = DataTypes.GetBooleanData(dr[item]);
                        }
                        break;

                    case "expirepasswordnow":
                        {
                            obj.ExpirePasswordNow = DataTypes.GetBooleanData(dr[item]);
                        }
                        break;
                    case "isshownintimelog":
                        {
                            obj.IsShownInTimeLog = DataTypes.GetBooleanData(dr[item]);
                        }
                        break;
                    case "receivelogisticsmessages":
                        {
                            obj.ReceiveLogisticsMessages = DataTypes.GetBooleanData(dr[item]);
                        }
                        break;
                    case "createuniversityuser":
                        {
                            obj.CreateUniversityUser = DataTypes.GetBooleanData(dr[item]);
                        }
                        break;
                    default:
                        break;
                };
            }

        }
    }
}
