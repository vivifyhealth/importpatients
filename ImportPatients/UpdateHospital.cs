﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;

namespace ImportPatients
{
    class UpdateHospital
    {
        public UpdateHospital()
        {
        }
        public void PostNewHospital(string file, bool create)
        {

            DataTable data = Common.GetData(file, true);
            List<DataTable> tables = UploadPatients.SplitTable(data, Convert.ToInt32(ConfigurationManager.AppSettings["batchSize"]));
            try
            {

                foreach (DataTable dt in tables)
                {
                    Parallel.ForEach(dt.AsEnumerable(), dr =>
                    {
                        var hospital = new DTO.PatientHospital();
                        string rowData = "";
                        int id = DataTypes.GetIntData(dr["autoID"]);

                        try
                        {
                            GetHospitalData(hospital, dr);

                            if (hospital.HospitalId > 0)
                            {
                                rowData = "RowNumber:" + id + " ," + hospital.Id + "," + hospital.HospitalId;
                                Logger.Write(rowData);
                                if (create)
                                {
                                    VivifyRestClient client = new VivifyRestClient();
                                    client.CallPatientHospitalApi(hospital, rowData);
                                }
                            }

                        }
                        catch (HttpResponseException unauthex)
                        {
                            Logger.Write("Error " + unauthex.Message);
                            tables.Clear();
                            dt.Clear();

                        }
                        catch (Exception ex)
                        {
                            Logger.Write("Error " + ex.Message);
                            Logger.Detail(ex.StackTrace);
                        }
                    });

                    Logger.Write("Do you wish to continue? Press X to exit.");
                    while (Console.ReadKey(true).Key == ConsoleKey.X)
                    {
                        Common.WriteToLog();
                        Environment.Exit(0);
                    }
                }
                Logger.Write("Upload Complete.");
            }
            catch (Exception ex)
            {
                Logger.Write("Error: " + ex.Message);
                Logger.Detail(ex.StackTrace);
            }
        }

        /// <summary>
        /// this call replaces any existing customfield, timezone, payerid,referralid etc to empty fields.
        /// </summary>
        /// <param name="pg"></param>
        /// <param name="dr"></param>
        private void GetHospitalData(DTO.PatientHospital pg, DataRow dr)
        {
            
            foreach (DataColumn item in dr.Table.Columns)
            {
                switch (item.ColumnName.ToLower())
                {
                    case "patientid":
                        {
                            pg.Id = (int)DataTypes.GetIntDataWithNull(dr[item]);

                        }
                        break;
                    case "hospitalid":
                        {
                            pg.HospitalId = (int)DataTypes.GetIntDataWithNull(dr[item]);

                        }
                        break;
                    case "patientmasterid":
                        {
                            pg.PatientMasterId = (int)DataTypes.GetIntDataWithNull(dr[item]);

                        }
                        break;
                    case "mrnident":
                        {
                            pg.MRNIdent = DataTypes.GetStringData(dr[item]);

                        }
                        break;
                    case "customfield":
                        {
                            pg.CustomField = DataTypes.GetStringData(dr[item]);

                        }
                        break;
                    case "payerid":
                        {
                            pg.PayerId = (int)DataTypes.GetIntDataWithNull(dr[item]);

                        }
                        break;
                    case "referralid":
                        {
                            pg.ReferralId= (int)DataTypes.GetIntDataWithNull(dr[item]);

                        }
                        break;
                    case "consultnumber":
                        {
                            pg.ConsultNumber = DataTypes.GetStringData(dr[item]);

                        }
                        break;
                    case "hl7custom1":
                        {
                            pg.HL7Custom1 = DataTypes.GetStringData(dr[item]);

                        }
                        break;
                    case "newpatientident":
                        {
                            pg.NewPatientIdent = DataTypes.GetStringData(dr[item]);

                        }
                        break;
                    case "empiident":
                        {
                            pg.EmpiIdent = DataTypes.GetStringData(dr[item]);

                        }
                        break;
                    case "timezoneid":
                        {
                            pg.TimeZoneId = (int)DataTypes.GetIntDataWithNull(dr[item]);

                        }
                        break;
                                    

                    default:
                        break;
                };
            }

        }
    }
}
